function RainDrop()
{
    this.position = new Vector2D(randomInt(0,canvas.height),randomInt(0,canvas.width));
    this.animation = rainAnimation.clone();
    this.speed = 5;
    this.amountToFall = randomInt(200,450);
    this.landing = false;
    this.delay = randomInt(0,5);
    this.reset = function()
    {
        this.delay = randomInt(0,5);
        var side = randomInt(0,2);
        this.amountToFall = randomInt(0,450);
        if(side === 0)
        {
            this.position.x = randomInt(-100,100);
            this.position.y = randomInt(0,canvas.height * 0.75);
        }
        else
        {
            this.position.y = randomInt(-100,100);
            this.position.x = randomInt(0,canvas.width * 0.75);
        }
        this.animation.gotoAndPlay("Fall");
    }
    this.update = function()
    {

        this.position = this.position.add({x:this.speed,y:this.speed});
        
        this.animation.x = this.position.x;
        this.animation.y = this.position.y;
        this.amountToFall -= this.speed;
        
   
        if(!this.landing && this.amountToFall <= 20)
        {
            this.animation.gotoAndPlay("Drop");
            this.landing = true;
        }
        else if(this.amountToFall <= 0 )
        {
            this.reset();
        }
        
    }
    
    this.addResources = function()
    {
        stage.addChild(this.animation);
    }
    
}
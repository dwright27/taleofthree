//making a class
function Classname()
{
    //variables and methods are listed with this.
    
    this.variableName;    
    this.variableName1;

    this.methodName = function ()
    {
        //do stuff
    }
}


//methods to be inheirited by a class follow 
Classname.prototype.inheirtedMethodName = function()
{
    // do stuff
}

function ChildClass()
{
    //constructor
    Parent.call(this)
    //child variables go here
    this.childVariableName;
    this.childVariableName1;
    this.childVariableName2;
    
}

ChildClass.prototype = Object.create(Classname.prototype);
ChildClass.prototype.constructor = ChildClass;

//override parent method 
ChildClass.prototype.inheirtedMethodName = function()
{
    //call base class implementation
    Parent.prototype.inheirtedMethodName.call(this);
    //do override logic 
    
}
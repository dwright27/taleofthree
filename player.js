function Player(animation,animationName, fallAnimation)
{
    Character.call(this,animation);
    this.health = 50;
    this.BASE_HEALTH = 50;
    this.attackRange;
    this.attackPower;

    
    
    this.fallDone = function()
    {
        
        
        this.off("animationend", this.fallDone);
        transitioning = false;  
        removeAnimation = true;

    }
    this.fallAnimation = fallAnimation.clone();
    this.fallAnimation.on("animationend", this.fallDone);
    
    
    this.animationName = animationName; 
//    this.dialogImage = this.dialogBox.image;
   
    this.keyDown = false;
    this.lastDirection = KEYCODE_UP;
    this.position = new Vector2D(345,545);
    this.speed = 5;
    this.healthDisplay = healthDisplay.clone();
    //this.dialogImage.gotoAndPlay(animationName)
    this.items = new Array();
    this.velocity = new Vector2D(0,0);
    
    this.healthText = new createjs.Text("100","24px Old English Text MT", "#FFF" );
    this.healthText.x = this.healthDisplay.x + 10;
    this.healthText.y = this.healthDisplay.y + 10;
    this.healthText.regX = -35;
    this.healthText.regY = -30;
    
    
    
    this.isUp = false;
    this.isDown = false;
    this.isRight = false;
    this.isLeft = false;
    this.isStanding = false;
    

    
    
    this.startPosition = new Vector2D(this.position.x,this.position.y);   
    this.reset = function()
    {
        this.health = this.BASE_HEALTH;
        this.position = new Vector2D(this.startPosition.x, this.startPosition.y);
    }
    
    this.adjustHealth = function()
    {
        var percent = (player.health/this.BASE_HEALTH) * 100;
        percent/=20;
        percent = Math.floor(percent);
        percent *=20;
        this.healthDisplay.gotoAndPlay("health" + percent);
        if(player.health <= this.BASE_HEALTH)
        this.healthText.text = player.health;
    }
    this.checkCollisions = function()
    {
        var tpos = new Vector2D(this.position.x,this.position.y);
        tpos.x = Math.floor(tpos.x / TileSizeX);
        tpos.y = Math.floor(tpos.y / TileSizeY);
        var t = TilesInfo[tpos.x-1][tpos.y];

            
                if ((this.position.x-(TileSizeX/2)) <= (tpos.x)*TileSizeX)
                {
                    if (TilesInfo[tpos.x-1][tpos.y] == Tile_NOT_PASSABLE)
                    {
                        this.position.x += this.speed;
                        
                    }
                }
     
                
        
    
                if ((this.position.x+(TileSizeX/2)) >= (tpos.x)*TileSizeX)
                {
                    if (TilesInfo[tpos.x+1][tpos.y] == Tile_NOT_PASSABLE)
                    {
                        this.position.x -= this.speed;
                        
                    }
					
                }
            
                
            
                if ((this.position.y-(TileSizeY/2)) <= (tpos.y)*TileSizeY)
                {
                    if (TilesInfo[tpos.x][tpos.y-1] == Tile_NOT_PASSABLE)
                    {
                        this.position.y += this.speed;
                        
                    }
					else if( isDoor(TilesInfo[tpos.x][tpos.y-1] , {x:tpos.x, y:tpos.y-1}))
					{
                        
                        var previousLevel = currentLevel;
				        currentLevel = convertToDoor(TilesInfo[tpos.x][tpos.y-1]);
                        

                        
                        this.position = getSpawnPoint(previousLevel,currentLevel);
                        setTilesToLevel(currentLevel);
                        if(previousLevel === 0)
                        {
                            transitioning = true;
                            //removeRainDrops();
                            transition();
                        }
					}
                }
            
                if ((this.position.y+(TileSizeY/2)) >= (tpos.y)*TileSizeY)
                {
                    if (TilesInfo[tpos.x][tpos.y+1] == Tile_NOT_PASSABLE)
                    {
                        this.position.y -= this.speed;
                        
                    }
					else if( isDoor(TilesInfo[tpos.x][tpos.y+1],{x:tpos.x, y:tpos.y+1}))
					{
                        var previousLevel = currentLevel;
 
                        
				        currentLevel = convertToDoor(TilesInfo[tpos.x][tpos.y+1]);
                        this.position = getSpawnPoint(previousLevel,currentLevel);
                        setTilesToLevel(currentLevel);
					}
                }
            
        
    }
    this.addItem = function(item)
    {
        items.push(item);
    }
    
    this.stand = function()
    {
    
        switch(this.lastDirection)
        {
                case KEYCODE_LEFT:
                    this.animation.gotoAndPlay("StandLeft");
                break;
                
                case KEYCODE_RIGHT:
                    this.animation.gotoAndPlay("StandRight");
                break;
                
                case KEYCODE_UP:
                    this.animation.gotoAndPlay("StandUp");
                break;
                
                case KEYCODE_DOWN:
                    this.animation.gotoAndPlay("StandDown");
                break;
        }
        this.isStanding = true;
    }
    
        this.walk = function()
    {
    
        switch(this.lastDirection)
        {
                case KEYCODE_LEFT:
                    this.animation.gotoAndPlay("WalkLeft");
                break;
                
                case KEYCODE_RIGHT:
                    this.animation.gotoAndPlay("WalkRight");
                break;
                
                case KEYCODE_UP:
                    this.animation.gotoAndPlay("WalkUp");
                break;
                
                case KEYCODE_DOWN:
                    this.animation.gotoAndPlay("WalkDown");
                break;
        }
        
    }
    
    this.useItem = function(item)
    {
//        if(this.containsItem(item))
//        {
            //item.use(this);
            this.adjustHealth();
            if(this.health > this.BASE_HEALTH)
            {
                this.health = this.BASE_HEALTH;
            }
  //          this.remove(item);
//        }
    };
    this.containsItem = function(item)
    {
        var found = false;
        for(var i = 0; i < this.items.length && !found; i++)
        {
            if(item == items[i])
            {
                
                found = true;
            }
        }
        return found;
    };
    
    this.removeItem = function(item)
    {
        var found = false;
        for(var i = 0; i < items.length && !found; i++)
        {
            if(item == items[i])
            {
                items.splice(i,1);
                found = true;
            }
        }
    };
    
    
    
    this.right = function()
    {
        this.velocity.x = this.speed;
        if((this.isLeft &&!this.isRight) || !this.isRight)
        {
        this.keyDown = true;
        this.animation.gotoAndPlay("WalkRight");
        this.lastDirection = KEYCODE_RIGHT;
            this.isLeft = false;
            this.isRight = true;
        }
    };
    
    this.up = function()
    {
        this.velocity.y = -this.speed;
        if((this.isDown &&!this.isUp) || !this.isUp)
        {
            this.keyDown = true;
            this.animation.gotoAndPlay("WalkUp");
            this.lastDirection = KEYCODE_UP;
            this.isDown = false;
            this.isUp = true;

        }
    }
    
    this.down = function()
    {
         this.velocity.y = this.speed; 
        
        if((this.isUp &&!this.isDown) || !this.isDown)
        {
            this.keyDown = true;
            this.animation.gotoAndPlay("WalkDown");
            this.lastDirection = KEYCODE_DOWN;
            this.isDown = true;
            this.isUp = false;

        }
    }
    
    this.left = function()
    {
        
        this.velocity.x = -this.speed; 
        
        if((this.isRight && !this.isLeft) || !this.isLeft)
        {
        this.keyDown = true;
        this.animation.gotoAndPlay("WalkLeft");
        this.lastDirection = KEYCODE_LEFT;
            this.isRight = false;
            this.isLeft = true;
           // this.position = this.position.add(new Vector2D(-this.speed,0));
        }

    };
    
    this.move = function()
    {
        this.position = this.position.add(this.velocity);
        if(!this.isStanding && !this.isUp&& !this.isRight && !this.isLeft && !this.isDown)
        {
            this.keyDown = false;
            this.stand();
            console.log("standing");
        }
    };
};


Player.prototype = Object.create(Character.prototype);
Player.prototype.constructor = Player;

Player.prototype.attack = function()
{
    switch(this.lastDirection)
    {
        case KEYCODE_LEFT:
            if(this.isStanding)
                this.animation.gotoAndPlay("AttackLeft");
            else
                this.animation.gotoAndPlay("AttackMoveLeft");

        break;
        
        case KEYCODE_RIGHT:
            if(this.isStanding)
                this.animation.gotoAndPlay("AttackRight");
            else
                this.animation.gotoAndPlay("AttackMoveRight");

        break;
        
        case KEYCODE_UP:
            if(this.isStanding)
                this.animation.gotoAndPlay("AttackUp");
            else
                this.animation.gotoAndPlay("AttackMoveUp");

        break;
        
        case KEYCODE_DOWN:
            if(this.isStanding)
                this.animation.gotoAndPlay("AttackDown");
            else
                this.animation.gotoAndPlay("AttackMoveDown");

        break;
    }
        
}


Player.prototype.update = function()
{
    Character.prototype.update.call(this);
    this.checkCollisions();
    this.move();
   // this.checkCollisions();
    if(player.health <= 0)
    {
        gameState = new GameOverState();
    }
    

};

Player.prototype.addResources = function()
{
    stage.addChild(this.animation);
    stage.addChild(this.healthDisplay);

    this.adjustHealth();
    stage.addChild(this.healthText);
    //this.healthDisplay.gotoAndPlay("health100");
}

Player.prototype.removeResources = function()
{
    stage.removeChild(this.animation);
    
}

Player.prototype.handleKeyUp = function(keyUp)
{
    

    
    switch(keyUp)
    {
     case KEYCODE_RIGHT:
            this.isRight = false;
            this.velocity.x = 0;
            break;
     case KEYCODE_LEFT: 
            this.isLeft = false;
            this.velocity.x = 0;
            break;
     case KEYCODE_DOWN:
            this.isDown = false;
            this.velocity.y = 0;
            break;
     case KEYCODE_UP:
            this.isUp = false;
            this.velocity.y = 0;

            break;
    }
};

Player.prototype.attacked = function(attackPower)
{
    if(!jKeyPressed)
    {
        this.health -= attackPower;
        this.adjustHealth();
        if(this.health <= 0)
        {
            this.removeResources();
            gameState = new GameOverState();
        }
    }
    
};

            
Player.prototype.handleKeyDown = function (keydown)
{
    //J KEY IS ON
    
    switch(keydown)
    {
            case KEYCODE_RIGHT:
            if(!this.isLeft)
            {
                this.isStanding = false;
                this.right();
            }
            break;
            case KEYCODE_LEFT:
            //move left;
            if(!this.isRight)
            {
                this.isStanding = false;
                this.left();
            }
            break;
            case KEYCODE_DOWN:
            if(!this.isUp)
            {
                this.isStanding = false;
                //move down;
                this.down();
            }
            break;
            case KEYCODE_UP:
            //move up;
            if(!this.isDown)
            {
                this.isStanding = false;
                this.up();
            }
            break;
    }
    
};


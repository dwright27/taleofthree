function Enemy(animation)
{
    Character.call(this, animation);
    this.attackPower = 5;
    this.health  =10;
    
    
}




Enemy.prototype = Object.create(Character.prototype);
Enemy.prototype.constructor = Enemy;


Enemy.prototype.randomDrop = function()
    {
        var  number = Math.floor(Math.random() * 100);
        if(number < 40)
        {
        var newPot = new Potion(5);
        newPot.position = new Vector2D(this.position.x, this.position.y);
        newPot.addResources();
        items.push(newPot);
        }
    }

Enemy.prototype.update = function()
{
    Character.prototype.update.call(this);
};

Enemy.prototype.attack = function()
{
    player.attacked(this.attackPower);
    
};


Enemy.prototype.attacked = function(attackPower)
{
    this.health-= attackPower;
    if(this.health <= 0)
    {
        this.removeResources();
        //stage.removeChild(this.animation);
        for(var i in enemies)
        {
            if(enemies[i] == this)
            {
                enemies.splice(i, 1);
                this.randomDrop();  
            }
        }
        
    }
    
}

Enemy.prototype.addResources = function()
{
    stage.addChild(this.animation);
    
}


Enemy.prototype.move = function()
{

};

Enemy.prototype.removeResources = function()
{
    stage.removeChild(this.animation);
}



function WarriorEnemy(position)
{
    this.attackPower = 10;
    this.speed = 2;
    this.range = 32;
    this.sightRange = 120;
    this.health = 20;
    this.position = position;
    this.animation = skeletonAnimation.clone();
    this.animation.gotoAndPlay("StandDown");
    this.LEFT = 0;
    this.RIGHT = 1;
    this.DOWN = 2;
    this.UP = 3;
    this.attackDelay = 0;
    this.ATTACK_DELAY_AMOUNT =2;
    this.lastDirection = this.RIGHT;
    
    this.moveLeft = function()
    {
        if(this.lastDirection != this.LEFT)
        {
            this.lastDirection = this.LEFT;
            this.animation.gotoAndPlay("WalkLeft");
        }
        
        this.position = this.position.add({x:-this.speed,y:0});
    };
    this.moveRight = function()
    {
        if(this.lastDirection != this.RIGHT)
        {
            this.lastDirection = this.RIGHT;
            this.animation.gotoAndPlay("WalkRight");
        }
        
        this.position = this.position.add({x:this.speed,y:0});
    
    };
    this.direction = this.moveLeft;
    this.distanceTraveled = 0;
    this.DISTANCE_TO_TRAVEL = 100;
    this.moveTowardPlayer = function()
    {
        var deltaPos = player.position.subtract(this.position);
        
        if(Math.abs(deltaPos.x) > Math.abs(deltaPos.y))
        {
            if(deltaPos.x > 0)
            {
                if(this.lastDirection != this.RIGHT)
                {
                    this.animation.gotoAndPlay("WalkRight");
                    this.lastDirection = this.RIGHT;
                }
                this.position = this.position.add({x:this.speed,y:0});

            }
            else
            {
                if(this.lastDirection != this.LEFT)
                {
                    this.animation.gotoAndPlay("WalkLeft");
                    this.lastDirection = this.LEFT;
                }
                                this.position = this.position.add({x:-this.speed,y:0});

            }
        }
        else
        {
            if(deltaPos.y > 0) 
            {
                if(this.lastDirection != this.DOWN)
                {
                    this.animation.gotoAndPlay("WalkDown");
                    this.lastDirection = this.DOWN;
                }
                                this.position = this.position.add({x:0,y:this.speed});

            }
            else
            {
                if(this.lastDirection != this.UP)
                {
                    this.animation.gotoAndPlay("WalkUp");
                    this.lastDirection = this.UP;
                }
                this.position = this.position.add({x:0,y:-this.speed});
            }
        }
    }
     this.checkCollisions = function()
    {
        var tpos = new Vector2D(this.position.x,this.position.y);
        tpos.x = Math.floor(tpos.x / TileSizeX);
        tpos.y = Math.floor(tpos.y / TileSizeY);
        var t = TilesInfo[tpos.x-1][tpos.y];
         

            
                if ((this.position.x-(TileSizeX/2)) <= (tpos.x)*TileSizeX)
                {
                    if (TilesInfo[tpos.x-1][tpos.y] == Tile_NOT_PASSABLE)
                    {
                        this.position.x += this.speed;
                        //this.direction = this.moveRight;
                        //this.distanceTraveled = 0;
                    }
                }
     
                
        
    
                if ((this.position.x+(TileSizeX/2)) >= (tpos.x)*TileSizeX)
                {
                    if (TilesInfo[tpos.x+1][tpos.y] == Tile_NOT_PASSABLE)
                    {
                        this.position.x -= this.speed;
                        //this.direction = this.moveLeft;
                        //this.distanceTraveled = 0;
                    }
					
                }
            
                
            
                if ((this.position.y-(TileSizeY/2)) <= (tpos.y)*TileSizeY)
                {
                    if (TilesInfo[tpos.x][tpos.y-1] == Tile_NOT_PASSABLE)
                    {
                        this.position.y += this.speed;
                        
                    }
                }
            
                if ((this.position.y+(TileSizeY/2)) >= (tpos.y)*TileSizeY)
                {
                    if (TilesInfo[tpos.x][tpos.y+1] == Tile_NOT_PASSABLE)
                    {
                        this.position.y -= this.speed;
                        
                    }
                }
            
        
    }
     
     this.attackInDirection = function()
     {
         switch(this.lastDirection)
         {
                 case this.LEFT:
                    this.animation.gotoAndPlay("AttackLeft");
                 break;
                case this.RIGHT:
                    this.animation.gotoAndPlay("AttackRight");
                 break;
                case this.UP:
                    this.animation.gotoAndPlay("AttackUp");
                 break;
                case this.DOWN:
                    this.animation.gotoAndPlay("AttackDown");
                 break;
         }
     }
}

WarriorEnemy.prototype = Object.create(Enemy.prototype);
WarriorEnemy.prototype.constructor = WarriorEnemy;

WarriorEnemy.prototype.update = function()
{
    Enemy.prototype.update.call(this);
    if(this.position.distance(player.position) < this.sightRange )
    {
        this.attack();
    }
    else
    {
        this.move();
    }
}

WarriorEnemy.prototype.move = function()
{
    this.checkCollisions();
    this.direction();
    this.distanceTraveled += this.speed;
    if(this.distanceTraveled > this.DISTANCE_TO_TRAVEL)
    {
        this.distanceTraveled = 0;
        this.direction = (this.lastDirection == this.LEFT) ? this.moveRight : this.moveLeft;
    }
    
}

WarriorEnemy.prototype.attack = function()
{
    this.checkCollisions();
    if(player.position.distance(this.position) < this.range)
    {
        
        if(this.attackDelay <= 0)
        {
            this.attackInDirection();
            this.attackDelay = this.ATTACK_DELAY_AMOUNT;
            player.attacked(this.attackPower);
        }
        else
        {
            this.attackDelay-=(1/FPS);
        }
    }
    else
    {
        this.moveTowardPlayer();
    }
}


function MobileEnemy(position)
{
    Enemy.call(this,floatingSkullAnimation.clone());
    this.attackPower = 2;
    this.speed = 100;
    this.range = 16;
    this.BASE_HEALTH = 5;
    this.health = this.BASE_HEALTH;
    this.position = position;
    this.animation.gotoAndPlay("StandDown");
}


MobileEnemy.prototype = Object.create(Enemy.prototype);
MobileEnemy.prototype.constructor = MobileEnemy;


MobileEnemy.prototype.update = function()
{
    Enemy.prototype.update.call(this);
     this.animation.gotoAndPlay("StandDown");
    this.move();
    if(this.range > player.position.distance(this.position))
    {
        this.attack();
        this.removeResources();
        for(var i in enemies)
        {
            if(enemies[i] == this)
            {
                enemies.splice(i, 1); 
            }
        }
        
    
    }
}

MobileEnemy.prototype.addResources = function()
{
    stage.addChild(this.animation);
    
}

MobileEnemy.prototype.removeResources = function()
{
    stage.removeChild(this.animation);
}

MobileEnemy.prototype.move = function()
{
    var resultDifference = player.position.subtract(this.position);
    resultDifference = resultDifference.normalize();
    resultDifference = resultDifference.multiply(this.speed);
    this.position = this.position.add(resultDifference);
}


function StationaryEnemy(animation, position, projectileAnimation)
{
    Enemy.call(this, animation);
    this.attackPower = 10;
    this.sightRange = 8;
	this.position = position;
	this.arrowPosition = new Vector2D(-32,-32);
	this.arrowVelocity = new Vector2D(0,0);
	this.arrowAnimation = projectileAnimation.clone();
	this.attackTimer = 50;
	if (!position)
	{
		position = new Vector2D(0,0);
	}
	this.animation.gotoAndPlay("StandDown");
	
	// frameCount += 1;
    // if(frameCount%(FPS/10) === 0) {
        // gameTimer = frameCount/(FPS);   
		// clockText.text = "Time: "+gameTimer+"s";
		// stage.update();
    // }
}

StationaryEnemy.prototype = Object.create(Enemy.prototype);
StationaryEnemy.prototype.constructor = StationaryEnemy;

StationaryEnemy.prototype.update = function()
{
    Enemy.prototype.update.call(this);
	this.move();
};

StationaryEnemy.prototype.attack = function()
{
    player.attacked(this.attackPower);
    
};
StationaryEnemy.prototype.addResources = function()
{
    Enemy.prototype.addResources.call(this);
    stage.addChild(this.arrowAnimation);
}

StationaryEnemy.prototype.removeResources = function ()
{
    //Enemy.prototype.removeResources.call(this);
    stage.removeChild(this.animation);
    stage.removeChild(this.arrowAnimation);
}

StationaryEnemy.prototype.move = function()
{
	if (Math.floor(player.position.distance(this.position)/32) < this.sightRange)
	{
        
		var dx = this.position.x - player.position.x;
		if (dx < 0)
		{
			dx = dx*-1;
		}
		var dy = this.position.y - player.position.y;
		if (dy < 0)
		{
			dy = dy*-1;
		}
//		if (dx >= dy)
//		{
//			if (player.position.x > this.position.x)
//			{
//				this.animation.gotoAndPlay("StandRight");
//			}
//			else
//			if (player.position.x < this.position.x)
//			{
//				this.animation.gotoAndPlay("StandLeft");
//			}
//		}
//		else
//		{
//			if (player.position.y < this.position.y)
//			{
//				this.animation.gotoAndPlay("StandUp");
//			}
//			else
//			if (player.position.y > this.position.y)
//			{
//				this.animation.gotoAndPlay("StandDown");
//			}
//		}
		
		this.attackTimer -= 1;
        
        if(this.attackTimer ==16)
        {
            this.animation.gotoAndPlay("PowerUp");
        }
            
        
		if (this.attackTimer <= 0)
		{
            this.animation.gotoAndPlay("StandDown");
			this.arrowPosition = new Vector2D(this.position.x,this.position.y);
			this.attackTimer = 50;
			this.arrowVelocity = new Vector2D((player.position.x -this.position.x)* (1/30) , (player.position.y - this.position.y)* (1/30));
			this.arrowVelocity.normalize();
		}
	}
    else
    {
        this.animation.gotoAndPlay("StandDown");
    }
	
	if (this.arrowAnimation)
	{
		this.arrowAnimation.x = this.arrowPosition.x;
		this.arrowAnimation.y = this.arrowPosition.y;
		this.arrowPosition = this.arrowPosition.add(this.arrowVelocity);
		
		var adx = this.arrowVelocity.x;
		var ady = this.arrowVelocity.y;
		
		if (ady < 1 && ady > -1)
			{
				if (adx < 0)
				{
					this.arrowAnimation.gotoAndPlay("Left");
				}
				else
				{
					this.arrowAnimation.gotoAndPlay("Right");
				}
			}
			else
			if (ady <= -1)
			{
				if (adx > -1 && adx < 1)
				{
					this.arrowAnimation.gotoAndPlay("Up");
				}
				else
				if (adx <= -1)
				{
					this.arrowAnimation.gotoAndPlay("UpLeft");
				}
				else
				if (adx > 1)
				{
					this.arrowAnimation.gotoAndPlay("UpRight");
				}
				
			}
			else
			if (ady >= 1)
			{
				if (adx > -1 && adx < 1)
				{
					this.arrowAnimation.gotoAndPlay("Down");
				}
				else
				if (adx <= -1)
				{
					this.arrowAnimation.gotoAndPlay("DownLeft");
				}
				else
				if (adx > 1)
				{
					this.arrowAnimation.gotoAndPlay("DownRight");
				}
				
			}
		
		
		if (player.position.distance(this.arrowPosition) < TileSizeX)
		{
            
			player.attacked(this.attackPower);
			this.arrowPosition = new Vector2D(-32,-32);
			this.arrowVelocity = new Vector2D(0,0);
		}
	}
};


//Tilestuffs
var caveSprites;
var roomSize = 20;
var Levels;
var Tiles;
var TilesInfo;
var currentLevel = 0;
var TileSizeX = 32;
var TileSizeY = 32;
var Tile_NOT_PASSABLE = "X";
var Tile_DOOR = "O";

var tileSets = Array();

function initTiles()
{
	initLevels();
	Tiles = new Array(roomSize);
	TilesInfo = new Array(roomSize);
    
    var caveSprite =new createjs.Sprite(caveSprites);
    
    tileSets.push(castleExteriorAnimation.clone());    

    tileSets.push(caveSprite.clone());
    tileSets.push(castleInteriorAnimation.clone());
    
	for (var i = 0; i < roomSize; i++) 
	{
		Tiles[i] = new Array(roomSize);
		TilesInfo[i] = new Array(roomSize);
	}
	
	var caveSprite =new createjs.Sprite(caveSprites);
	for (var i = 0; i < roomSize; i++) 
	{
		for (var j = 0; j < roomSize; j++) 
		{
			Tiles[i][j] = caveSprite.clone();
			Tiles[i][j].x = TileSizeX*i;
			Tiles[i][j].y = (TileSizeY*j);
			stage.addChild(Tiles[i][j]);
		}
	}
	setTilesToLevel(0);
}

function setTileToImage(image)
{
    for (var i = 0; i < roomSize; i++) 
	{
		for (var j = 0; j < roomSize; j++) 
		{
            stage.removeChild(Tiles[i][j]);
        }
    }
    
    for (var i = 0; i < roomSize; i++) 
	{
		for (var j = 0; j < roomSize; j++) 
		{
			Tiles[i][j] = image.clone();
			Tiles[i][j].x = TileSizeX*i;
			Tiles[i][j].y = (TileSizeY*j);
			stage.addChild(Tiles[i][j]);
		}
	}
}

function initLevels()
{
	Levels = new Array(10);
	for (var i = 0; i < 10; i++)
	{
		Levels[i] = new Array(roomSize*2);
	}
	
	 Levels[0][0]  ="cbcbcbcbcdefgbcbcbcb";
    Levels[0][1]  = "jijijijijklmnijijiji";
    Levels[0][2]  = "qpqpqpqpqrstupqpqpqp";
    Levels[0][3]  = "xwxwxwxwxyzABwxwxwxw";
    Levels[0][4]  = "FaaaaaaaaaGaaaaaaaaF";
	Levels[0][5]  = "FaaaaaaaaaaaaaaaaaaF";
    Levels[0][6]  = "FaaSaaeaahijaaaaaaaF";
    Levels[0][7]  = "FaaaaaaaaopqaaGaaaaF";
    Levels[0][8]  = "FaaaaeaaavwxaaaaaaaF";
    Levels[0][9]  = "FaaaaaaaaCDEaaZaaaaF";
    Levels[0][10] = "FaaaaaaaaJKLaaaaaaaF";
    Levels[0][11] = "FaaaaaeaaaaaaaaaaaaF";
    Levels[0][12] = "FaaaaaaaeaaaaaaaGaaF";
    Levels[0][13] = "FaaaSaaaaaaaaaaaaaaF";
    Levels[0][14] = "FaaaaaeaaaaaaaaaaaaF";
    Levels[0][15] = "FaaaaaaaaaSaaaaaaaaF";
    Levels[0][16] = "FaaaaaaaaaaaaaaaGaaF";
    Levels[0][17] = "FaaaZaaaaaaaaaaaaaaF";
    Levels[0][18] = "FaaaaaaaaeaaaaaaaaaF";
    Levels[0][19] = "FFFFFFFFFFFFFFFFFFFF";

	
	Levels[0][20] = "XXXXXXXXXXXXXXXXXXXX";
	Levels[0][21] = "XXXXXXXXXXXXXXXXXXXX";
	Levels[0][22] = "XXXXXXXXXXXXXXXXXXXX";
	Levels[0][23] = "XXXXXXXXXXXXXXXXXXXX";
	Levels[0][24] = "X                  X";
	Levels[0][25] = "X                  X";
	Levels[0][26] = "X                  X";
	Levels[0][27] = "X        XXX       X";
	Levels[0][28] = "X        XXX       X";
	Levels[0][29] = "X        XXX       X";
	Levels[0][30] = "X        XXX       X";
	Levels[0][31] = "X                  X";
	Levels[0][32] = "X                  X";
	Levels[0][33] = "X                  X";
	Levels[0][34] = "X                  X";
	Levels[0][35] = "X                  X";
	Levels[0][36] = "X                  X";
	Levels[0][37] = "X                  X";
	Levels[0][38] = "X                  X";
	Levels[0][39] = "XXXXXXXXXXXXXXXXXXXX";
	
    
    
//    Levels[0][0]  = "FOPHFHHFHHHFHHHFHHFH";
//    Levels[0][1]  = "BOPaaaaaaGaaaaaaaaaH";
//    Levels[0][2]  = "FVWaaaaaaaaaaaaaaaaB";
//    Levels[0][3]  = "HaaaaaSaaaaaaaZaaaaF";
//    Levels[0][4]  = "FaaaaaaaaaGaaaaaaaaB";
//	Levels[0][5]  = "daaaaaaaaaaaaaaaaaad";
//    Levels[0][6]  = "HaaSaaeaahijaaaaaaaB";
//    Levels[0][7]  = "FaaaaaaaaopqaaGaaaaB";
//    Levels[0][8]  = "BaaaaeaaavwxaaaaaaaF";
//    Levels[0][9]  = "FaaaaaaaaCDEaaZaaaaB";
//    Levels[0][10] = "FaaaaaaaaJKLaaaaaaaB";
//    Levels[0][11] = "HaaaaaeaaaaaaaaaaaaH";
//    Levels[0][12] = "FaaaaaaaeaaaaaaaGaaB";
//    Levels[0][13] = "BaaaSaaaaaaaaaaaaaaF";
//    Levels[0][14] = "FaaaaaeaaaaaaaaaaaaB";
//    Levels[0][15] = "FaaaaaaaaaSaaaaaaaaH";
//    Levels[0][16] = "HaaaaaaaaaaaaaaaGaaB";
//    Levels[0][17] = "FaaaZaaaaaaaaaaaaaaB";
//    Levels[0][18] = "BaaaaaaaaeaaaaaaaaaF";
//    Levels[0][19] = "HQBQHQBQQHQBQQBHQBBH";
//
//	
//	Levels[0][20] = "XXXXXXXXXXXXXXXXXXXX";
//	Levels[0][21] = "XXX                X";
//	Levels[0][22] = "X11                X";
//	Levels[0][23] = "X                  X";
//	Levels[0][24] = "X                  X";
//	Levels[0][25] = "X                  X";
//	Levels[0][26] = "X                  X";
//	Levels[0][27] = "X        XXX       X";
//	Levels[0][28] = "X        XXX       X";
//	Levels[0][29] = "X        XXX       X";
//	Levels[0][30] = "X        XXX       X";
//	Levels[0][31] = "X                  X";
//	Levels[0][32] = "X                  X";
//	Levels[0][33] = "X                  X";
//	Levels[0][34] = "X                  X";
//	Levels[0][35] = "X                  X";
//	Levels[0][36] = "X                  X";
//	Levels[0][37] = "X                  X";
//	Levels[0][38] = "X                  X";
//	Levels[0][39] = "XXXXXXXXXXXXXXXXXXXX";
//	
	Levels[1][0]  = "dddddddddddddddddddd";
	Levels[1][1]  = "daaaaaaaaaaaaaaaaaad";
	Levels[1][2]  = "daaaaaaaaaaaaaaaaaad";
	Levels[1][3]  = "daaaaaaaaaaaaaaaaaad";
	Levels[1][4]  = "daaataaaaaaaaaaataad";
	Levels[1][5]  = "daaaAaaaaaaaaaaaAaad";
	Levels[1][6]  = "daaaaaaaaaaaaaaaaaad";
	Levels[1][7]  = "daaaaaaaaaaaaaaaaaad";
	Levels[1][8]  = "daaaaaaaaaaaaaaaaaad";
	Levels[1][9]  = "daaaaaaaaaaaaaaaaaad";
	Levels[1][10] = "daaaaaaaaaaaaaaaaaad";
	Levels[1][11] = "daaaaaaaaaaaaaaaaaad";
	Levels[1][12] = "daaaaaaaaaaaaaaaaaad";
	Levels[1][13] = "daaataaaaaaaaaaataad";
	Levels[1][14] = "daaaAaaaaaaaaaaaAaad";
	Levels[1][15] = "daaaaaaaaaaaaaaaaaad";
	Levels[1][16] = "daaaaaaaaaaaaaaaaaad";
	Levels[1][17] = "daaaaaaaaaaaaaaaaaad";
	Levels[1][18] = "daaaaaaaaaaaaaaaaMNd";
	Levels[1][19] = "dddddddddddddddddTUd";
	
	Levels[1][20] = "XXXXXXXXXXXXXXXXXXXX";
	Levels[1][21] = "X                  X";
	Levels[1][22] = "X                  X";
	Levels[1][23] = "X                  X";
	Levels[1][24] = "X   X           X  X";
	Levels[1][25] = "X   X           X  X";
	Levels[1][26] = "X                  X";
	Levels[1][27] = "X                  X";
	Levels[1][28] = "X                  X";
	Levels[1][29] = "X                  X";
	Levels[1][30] = "X                  X";
	Levels[1][31] = "X                  X";
	Levels[1][32] = "X                  X";
	Levels[1][33] = "X   X           X  X";
	Levels[1][34] = "X   X           X  X";
	Levels[1][35] = "X                  X";
	Levels[1][36] = "X                  X";
	Levels[1][37] = "X                  X";
	Levels[1][38] = "X                00X";
	Levels[1][39] = "XXXXXXXXXXXXXXXXXXXX";
}

function convertToDoor(level)
{
    return parseInt(level);
}

function isDoor(level)
{
    var number = parseInt(level);
    
    var returnValue = !isNaN(number);
    return returnValue;
}

function setTilesToLevel(levelNumber)
{
    setTileToImage(tileSets[levelNumber]);
	for (var i = 0; i < roomSize; i++) 
	{
		for (var j = 0; j < roomSize; j++) 
		{
			Tiles[i][j].gotoAndPlay(Levels[levelNumber][j].charAt(i));
			TilesInfo[i][j] = Levels[levelNumber][j+roomSize].charAt(i);
		}
	}
	
}

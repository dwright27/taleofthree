var playerWon = false;
//Game States    
function GameState()
{
    
}

//GameState.prototype.initialize = function() {}
GameState.prototype.update = function() 
{
    
}
GameState.prototype.setup = function()
{
    stage.removeAllChildren();
    this.playMusic();
    update = this.update;
}
GameState.prototype.playMusic = function()
{
    if(!muted)
    {
        createjs.Sound.stop();
    }
}



function TitleState()
{
    this.setup();
}

TitleState.prototype = Object.create(GameState.prototype);
TitleState.prototype.constructor = TitleState;
TitleState.prototype.playMusic = function(){ 
   GameState.prototype.playMusic.call(this);

    if(!muted)
    {
        
        createjs.Sound.play("introductionMusic",{interrupt: createjs.Sound.INTERRUPT_ANY, loop:-1});
    }
}
TitleState.prototype.setup = function()
{
    GameState.prototype.setup.call(this);
    
    stage.addChild(titleScreen);
    stage.addChild(playButton);
    stage.addChild(creditsButton);
    stage.addChild(instructionsButton);
    stage.addChild(playButton.txt);
    stage.addChild(creditsButton.txt);
    stage.addChild(instructionsButton.txt);
    
}

function CreditsState()
{
    this.setup();   
}

CreditsState.prototype = Object.create(GameState.prototype);
CreditsState.prototype.playMusic = function(){
          GameState.prototype.playMusic.call(this);

    if(!muted)
    {
       
        createjs.Sound.play("introductionMusic",{interrupt: createjs.Sound.INTERRUPT_ANY, loop:-1});
    }
}

CreditsState.prototype.constructor = CreditsState;
CreditsState.prototype.setup = function()
{
    GameState.prototype.setup.call(this);
    stage.addChild(creditScreen);
    stage.addChild(mainMenuButton);
	
	var iceSwordLink = new createjs.Text("Ice Sword", "24px Old English Text MT", "#00f");
	iceSwordLink.addEventListener("click", iceSwordAndMace);
	iceSwordLink.x = 32;
	iceSwordLink.y = 200;
	var iceSwordBy = new createjs.Text("                  by           used under                              /Changed hilt \ncolor to match blade color & also broke it into pieces.", "24px Old English Text MT", "#000");
	iceSwordBy.x = 32;
	iceSwordBy.y = 200;
	var iceSwordAuthorLink = new createjs.Text("Bluce", "24px Old English Text MT", "#00f");
	iceSwordAuthorLink.addEventListener("click", bluce);
	iceSwordAuthorLink.x = 165;
	iceSwordAuthorLink.y = 200;
	var iceSwordlicense = new createjs.Text("CC-BY-SA 3.0", "24px Old English Text MT", "#00f"); 
	iceSwordlicense.x = 325;
	iceSwordlicense.y = 200;
	iceSwordlicense.addEventListener("click", CCBYSALicense);
	
	var iceMaceLink = new createjs.Text("Ice Mace", "24px Old English Text MT", "#00f");
	iceMaceLink.x = 32;
	iceMaceLink.y = 264;
	iceMaceLink.addEventListener("click", iceSwordAndMace);
	var iceMaceAuthorLink = iceSwordAuthorLink.clone();
	iceMaceAuthorLink.y = 264;
	iceMaceAuthorLink.addEventListener("click", bluce);
	var iceMaceBy = new createjs.Text("                 by            used under", "24px Old English Text MT", "#000");
	iceMaceBy.x = 32;								  
	iceMaceBy.y = 264;
	var iceMaceLicense = iceSwordlicense.clone();
	iceMaceLicense.y = 264;
	iceMaceLicense.addEventListener("click", CCBYSALicense);
	
	var shieldLink = new createjs.Text("Fire & Ice Shield", "24px Old English Text MT", "#00f");
	shieldLink.x = 32;
	shieldLink.y = 296;
	shieldLink.addEventListener("click", shield);
	var shieldAuthor = iceSwordAuthorLink.clone();
	shieldAuthor.x = 545;
	shieldAuthor.y = 296;
	shieldAuthor.addEventListener("click", bluce);
	var shieldBy = new createjs.Text("                              is a derivative of                           by \nused under                              and it is liccensed under the same \nlicense by Alfredo De Leon.", "24px Old English Text MT", "#000");
	shieldBy.x = 32;								  
	shieldBy.y = 296;
	var fireIceAxe = new createjs.Text("Flame/Ice Axe", "24px Old English Text MT", "00f");
	fireIceAxe.x = 360;
	fireIceAxe.y = 296;
	fireIceAxe.addEventListener("click", shield);
	var shieldLicense = iceSwordlicense.clone();
	shieldLicense.x = 130;
	shieldLicense.y = 328;
	shieldLicense.addEventListener("click", CCBYSALicense);
	
	var archerSoundLink = new createjs.Text("Archers Shooting", "24px Old English Text MT", "#00f");
	archerSoundLink.x = 32;
	archerSoundLink.y = 392;
	archerSoundLink.addEventListener("click", archerSound);
	var archerSoundAuthorLink = new createjs.Text("copyc4t", "24px Old English Text MT", "#00f");
	archerSoundAuthorLink.x = 230;
	archerSoundAuthorLink.y = 392;
	archerSoundAuthorLink.addEventListener("click", copyc4t);
	var archerSoundBy = new createjs.Text("                 by             used under", "24px Old English Text MT", "#000");
	archerSoundBy.x = 100;								  
	archerSoundBy.y = 392;
	var archerSoundLicense = new createjs.Text("CC-BY 3.0", "24px Old English Text MT", "#00f"); 
	archerSoundLicense.x = 400;
	archerSoundLicense.y = 392;
	archerSoundLicense.addEventListener("click", CCBYLicense);
	
	var shieldSoundLink = new createjs.Text("Shield Clank", "24px Old English Text MT", "#00f");
	shieldSoundLink.x = 32;
	shieldSoundLink.y = 424;
	shieldSoundLink.addEventListener("click", shieldSound);
	var shieldSoundAuthorLink = new createjs.Text("Brandon Morris", "24px Old English Text MT", "#00f");
	shieldSoundAuthorLink.x = 180;
	shieldSoundAuthorLink.y = 424;
	shieldSoundAuthorLink.addEventListener("click", brandon);
	var shieldSoundBy = new createjs.Text("          by                          used under", "24px Old English Text MT", "#000");
	shieldSoundBy.x = 100;								  
	shieldSoundBy.y = 424;
	var shieldSoundLicense = new createjs.Text("CC-BY 3.0", "24px Old English Text MT", "#00f"); 
	shieldSoundLicense.x = 430;
	shieldSoundLicense.y = 424;
	shieldSoundLicense.addEventListener("click", CCBYLicense);
	
    stage.addChild(iceSwordLink, iceSwordBy, iceSwordAuthorLink, iceSwordlicense, iceMaceLink, iceMaceAuthorLink, iceMaceBy, iceMaceLicense, shieldLink, shieldBy, fireIceAxe, shieldAuthor, shieldLicense, archerSoundLink, archerSoundAuthorLink, archerSoundBy, archerSoundLicense, shieldSoundLink, shieldSoundAuthorLink, shieldSoundBy, shieldSoundLicense);
	
	var backgroundSoundLink = new createjs.Text("StrumStrumStrum", "24px Old English Text MT", "#00f");
	backgroundSoundLink.addEventListener("click", backgroundSound);
	backgroundSoundLink.x = 32;
	backgroundSoundLink.y = 456;
	var backgroundSoundBy = new createjs.Text("                               by              used under                     \n&                              &                  & ", "24px Old English Text MT", "#000");
	backgroundSoundBy.x = 32;
	backgroundSoundBy.y = 456;
	var backgroundsoundAuthor = new createjs.Text("Botanic", "24px Old English Text MT", "#00f");
	backgroundsoundAuthor.addEventListener("click", botanic);
	backgroundsoundAuthor.x = 240;
	backgroundsoundAuthor.y = 456;
	var backgroundLicense1 = new createjs.Text("CC-BY 3.0", "24px Old English Text MT", "#00f"); 
	backgroundLicense1.x = 420;
	backgroundLicense1.y = 456;
	backgroundLicense1.addEventListener("click", CCBYLicense);
	
	var backgroundLicense2 = new createjs.Text("CC-BY-SA 3.0", "24px Old English Text MT", "#00f"); 
	backgroundLicense2.x = 50;
	backgroundLicense2.y = 488;
	backgroundLicense2.addEventListener("click", CCBYSALicense);
	
	var backgroundLicense3 = new createjs.Text("GPL 3.0", "24px Old English Text MT", "#00f"); 
	backgroundLicense3.x = 250;
	backgroundLicense3.y = 488;
	backgroundLicense3.addEventListener("click", GPL3);
	
	var backgroundLicense4 = new createjs.Text("GPL 2.0", "24px Old English Text MT", "#00f"); 
	backgroundLicense4.x = 380;
	backgroundLicense4.y = 488;
	backgroundLicense4.addEventListener("click", GPL2);
	
	
	
	
	stage.addChild(backgroundSoundLink, backgroundSoundBy, backgroundsoundAuthor, backgroundLicense1, backgroundLicense2, backgroundLicense3, backgroundLicense4);
	
	var tilesetLink = new createjs.Text("All Tile Terrains", "24px Old English Text MT", "#00f");
	tilesetLink.addEventListener("click", adrix89);
	tilesetLink.x = 32;
	tilesetLink.y = 520;
	var tilesetBy = new createjs.Text("                              by              used under                       \n&       ", "24px Old English Text MT", "#000");
	tilesetBy.x = 32;
	tilesetBy.y = 520;
	var tilesetAuthor = new createjs.Text("adrix89", "24px Old English Text MT", "#00f");
	tilesetAuthor.addEventListener("click", adrix89);
	tilesetAuthor.x = 240;
	tilesetAuthor.y = 520;
	var tilesetLicense1 = new createjs.Text("CC-BY-SA 3.0", "24px Old English Text MT", "#00f"); 
	tilesetLicense1.x = 410;
	tilesetLicense1.y = 520;
	tilesetLicense1.addEventListener("click", CCBYSALicense);
	
	var tilesetLicense2 = new createjs.Text("GPL 3.0", "24px Old English Text MT", "#00f"); 
	tilesetLicense2.x = 55;
	tilesetLicense2.y = 552;
	tilesetLicense2.addEventListener("click", GPL3);
	
	stage.addChild(tilesetLink, tilesetBy, tilesetAuthor, tilesetLicense1, tilesetLicense2);
}

function iceSwordAndMace(){
	goURL("Ice Sword & Mace");
}

function shieldSound(){
	goURL("Shield Sound");
}

function backgroundSound() {
	goURL("background");
}

function botanic() {
	goURL("Botanic");
}

function cc() {
	goURL("Botanic");
}

function archerSound(){
	goURL("Archer Sound");
}

function shield(){
	goURL("Shield");
}

function brandon() {
	goURL("Brandon Morris");
}

function adrix89() {
	goURL("adrix");
}
function copyc4t(){
	goURL("copyc4t");
}

function bluce(){
	goURL("Bluce");
}

function CCBYSALicense(){
	goURL("CC-BY-SA 3.0");
}

function CCBYLicense(){
	goURL("CC-BY 3.0");
}

function GPL3() {
	goURL("GPL 3.0");
}

function GPL2() {
	goURL("GPL 2.0");
}

function goURL(index) {
	if(index == "Ice Sword & Mace") {
		window.open("http://opengameart.org/content/flame-ice-sword-and-mace", "about:blank");
	}
	else if(index == "Shield") {
		window.open("http://opengameart.org/content/flame-ice-axe", "about:blank");
	}
	else if(index == "Bluce") {
		window.open("http://opengameart.org/users/bluce", "about:blank");
	}
	else if(index == "CC-BY-SA 3.0") {
		window.open("http://creativecommons.org/licenses/by-sa/3.0/", "about:blank");
	}
	else if(index == "CC-BY 3.0") {
		window.open("http://creativecommons.org/licenses/by/3.0/", "about:blank");
	}
	else if(index == "Archer Sound") {
		window.open("http://opengameart.org/content/archers-shooting", "about:blank");
	}
	else if(index == "copyc4t") {
		window.open("http://opengameart.org/users/copyc4t", "about:blank");
	}
	else if(index == "Shield Sound") {
		window.open("http://opengameart.org/content/osare-10-sound-pack", "about:blank");
	}
	else if(index == "Brandon Morris") {
		window.open("http://opengameart.org/users/haeldb", "about:blank");
	}
	else if(index == "background") {
		window.open("http://opengameart.org/content/strumstrumstrum", "about:blank");
	}
	else if(index == "Botanic") {
		window.open("http://opengameart.org/users/botanic", "about:blank");
	}
	else if(index == "GPL 3.0") {
		window.open("http://www.gnu.org/licenses/gpl-3.0.html", "about:blank");
	}
	else if(index == "GPL 2.0") {
		window.open("http://www.gnu.org/licenses/old-licenses/gpl-2.0.html", "about:blank");
	}
	else if(index == "adrix") {
		window.open("http://opengameart.org/users/adrix89", "about:blank");
	}
	
	
	window.focus();
}

function PlayGameState()
{
   this.setup();
}

PlayGameState.prototype = Object.create(GameState.prototype);
PlayGameState.prototype.constructor = PlayGameState;
PlayGameState.prototype.playMusic = function(){
          GameState.prototype.playMusic.call(this);

        if(!muted)
    {
      
    createjs.Sound.play("backgroundMusic",{interrupt: createjs.Sound.INTERRUPT_ANY, loop:-1});
    }
}

PlayGameState.prototype.setup = function()
{
    GameState.prototype.setup.call(this);
    playerWon = false;
    currentLevel = 0;
    initTiles();
   // stage.addChild(backgroundScreen);
   // stage.addChild(dialogBox.container);
    //initializeEnemies();
    for(var i in enemies)
    {
        enemies[i].addResources();
    }
	
	if(typeof player != "undefined") {
		player.removeResources();	
	}
	
    player.addResources();
    player.reset();
    addRainDrops();
	//stage.addChild(healthDisplay);    
}
var removeAnimation = false;
PlayGameState.prototype.update = function () 
{
    GameState.prototype.update.call(this);

    if(!transitioning)
    {
        
        updateRainDrops();    
    
        player.update();

        for(var i in enemies)
        {
            enemies[i].update();
        }

        for(var i in items)
        {
            items[i].update();
        }
    }
    
    if(removeAnimation)
    {
        removeAnimation = false;
        removeRainDrops();
        stage.removeChild(player.fallAnimation);
        stage.removeChild(fallScreenImage);
        
    }
}

function SelectCharacterState()
{
	this.setup();
}

SelectCharacterState.prototype = Object.create(GameState.prototype);
SelectCharacterState.prototype.playMusic = function(){
		GameState.prototype.playMusic.call(this);
	if(!muted)
	{
		createjs.Sound.play("introductionMusic",{interrupt: createjs.Sound.INTERRUPT_ANY, loop:-1});
	}
}

SelectCharacterState.prototype.constructor = SelectCharacterState;
SelectCharacterState.prototype.setup = function()
{
	GameState.prototype.setup.call(this);
	var wClone = warriorAnimation.clone();
	var mClone = mageAnimation.clone();
	var bClone = bowmanAnimations.clone();
	wClone.gotoAndPlay("WalkDown");
	wClone.x = 320;
	wClone.y = 330;
	mClone.gotoAndPlay("WalkDown");
	mClone.x = 140;
	mClone.y = 330;
	bClone.gotoAndPlay("WalkDown");
	bClone.x = 495;
	bClone.y = 330;
	stage.addChild(selectionScreen);
	stage.addChild(mainMenuButton);
	stage.addChild(selectWarriorButton, selectBowmanButton, selectMageButton);
	stage.addChild(mClone, wClone,bClone);
}

function StoryState(charactername)
{
	this.setup(charactername);
	this.story;
}

StoryState.prototype = Object.create(GameState.prototype);
StoryState.prototype.playMusic = function(){
		GameState.prototype.playMusic.call(this);
	if(!muted)
	{
		createjs.Sound.play("introductionMusic",{interrupt: createjs.Sound.INTERRUPT_ANY, loop:-1});
	}
}

StoryState.prototype.constructor = StoryState;
StoryState.prototype.setup = function(selectedCharacter)
{
	GameState.prototype.setup.call(this);
	
	if(selectedCharacter === "Warrior") {
		this.story = new createjs.Text("On a peaceful day your wife \nwas wifenapped by a \nmysterious hooded man. After \nweeks of searching for her \nyou finally discovered her \nwhereabouts and decided to \ngo rescue her.", "24px Old English Text MT", "#fff");
		player = new Warrior();
	}
	else if(selectedCharacter === "Mage") {
		this.story = new createjs.Text("On a peaceful day your spellbook \nwas booknapped by a \nmysterious hooded man. After \nweeks of searching for it \nyou finally discovered its \nwhereabouts and decided to \ngo recover it.", "24px Old English Text MT", "#fff");
		player = new Mage();
	}
	else if(selectedCharacter === "Bowman") {
		this.story = new createjs.Text("On a peaceful day your treasure \nwas treasurenapped by a \nmysterious hooded man. After \nweeks of searching for it \nyou finally discovered its \nwhereabouts and decided to \ngo recover it.", "24px Old English Text MT", "#fff");
		player = new Archer();
	}
	
	this.story.x = 200;
	this.story.y = 640;
	
	stage.addChild(fallScreenImage);
	stage.addChild(this.story);
}

StoryState.prototype.update = function() {
	this.story.y -= 2;
	
	if(this.story.y <= -300)
		gameState = new PlayGameState();
}

function InstructionsState()
{
    this.setup();
}


InstructionsState.prototype = Object.create(GameState.prototype);
InstructionsState.prototype.playMusic = function(){
          GameState.prototype.playMusic.call(this);

    if(!muted)
    {
       
        createjs.Sound.play("introductionMusic",{interrupt: createjs.Sound.INTERRUPT_ANY, loop:-1});
    }
}

InstructionsState.prototype.constructor = InstructionsState;
InstructionsState.prototype.setup = function()
{
    GameState.prototype.setup.call(this);
	var wClone = warriorAnimation.clone();
	var mClone = mageAnimation.clone();
	var bClone = bowmanAnimations.clone();
	wClone.gotoAndPlay("WalkDown");
	wClone.x = 320;
	wClone.y = 330;
	mClone.gotoAndPlay("WalkDown");
	mClone.x = 140;
	mClone.y = 330;
	bClone.gotoAndPlay("WalkDown");
	bClone.x = 495;
	bClone.y = 330;
    stage.addChild(instructionScreen);
    stage.addChild(mainMenuButton);
	stage.addChild(warriorInstructionsButton, mageInstructionsButton, bowmanInstructionsButton);
	stage.addChild(mClone, wClone,bClone);
}

function WarriorInstructionsState()
{
	this.setup();
}

WarriorInstructionsState.prototype = Object.create(GameState.prototype);
WarriorInstructionsState.prototype.playMusic = function() {
	GameState.prototype.playMusic.call(this);
	
	if(!muted)
	{
		createjs.Sound.play("introductionMusic",{interrupt: createjs.Sound.INTERRUPT_ANY, loop:-1});	
	}
}

WarriorInstructionsState.prototype.constructor = WarriorInstructionsState;
WarriorInstructionsState.prototype.setup = function()
{
	GameState.prototype.setup.call(this);
	var wClone = warriorAnimation.clone();
	var aClone = warriorAnimation.clone();
	var sClone = warriorAnimation.clone();
	var dClone = warriorAnimation.clone();
	var jClone = warriorAnimation.clone();
	var kClone = warriorAnimation.clone();
	wClone.gotoAndPlay("WalkUp");
	wClone.x = 160;
	wClone.y = 145;
	aClone.gotoAndPlay("WalkLeft");
	aClone.x = 160;
	aClone.y = 205;
	sClone.gotoAndPlay("WalkDown");
	sClone.x = 160;
	sClone.y = 265;
	dClone.gotoAndPlay("WalkRight");
	dClone.x = 160;
	dClone.y = 325;
	jClone.gotoAndPlay("AttackAll");
	jClone.x = 160;
	jClone.y = 385;
	kClone.gotoAndPlay("BlockAll");
	kClone.x = 160;
	kClone.y = 445;
	
	stage.addChild(warriorInstructionsScreen);
	stage.addChild(instructionsButton, mainMenuButton);
	stage.addChild(wClone, aClone, sClone, dClone, jClone, kClone);
}

function MageInstructionsState()
{
	this.setup();
}

MageInstructionsState.prototype = Object.create(GameState.prototype);
MageInstructionsState.prototype.playMusic = function() {
	GameState.prototype.playMusic.call(this);
	
	if(!muted)
	{
		createjs.Sound.play("introductionMusic",{interrupt: createjs.Sound.INTERRUPT_ANY, loop:-1});
	}
}

MageInstructionsState.prototype.constructor = MageInstructionsState;
MageInstructionsState.prototype.setup = function()
{
	GameState.prototype.setup.call(this);
	var wClone = mageAnimation.clone();
	var aClone = mageAnimation.clone();
	var sClone = mageAnimation.clone();
	var dClone = mageAnimation.clone();
	var jClone = mageAnimation.clone();
	var jSpell = mageSpellAnimations.clone();
	var kClone = mageAnimation.clone();
	var kSpell = mageSpellAnimations.clone();
	wClone.gotoAndPlay("WalkUp");
	wClone.x = 160;
	wClone.y = 145;
	aClone.gotoAndPlay("WalkLeft");
	aClone.x = 160;
	aClone.y = 215;
	sClone.gotoAndPlay("WalkDown");
	sClone.x = 160;
	sClone.y = 285;
	dClone.gotoAndPlay("WalkRight");
	dClone.x = 160;
	dClone.y = 355;
	jClone.gotoAndPlay("AttackAll");
	jClone.x = 160;
	jClone.y = 425;
	jSpell.gotoAndPlay("FireLoop");
	jSpell.x = 160;
	jSpell.y = 425;
	kClone.gotoAndPlay("AttackAll");
	kClone.x = 160;
	kClone.y = 495;
	kSpell.gotoAndPlay("IceLoop");
	kSpell.x = 160;
	kSpell.y = 495;
	
	stage.addChild(mageInstructionsScreen);
	stage.addChild(instructionsButton, mainMenuButton);
	stage.addChild(wClone, aClone, sClone, dClone, jSpell, jClone, kSpell, kClone);
}

function BowmanInstructionsState()
{
	this.setup();
}

BowmanInstructionsState.prototype = Object.create(GameState.prototype);
BowmanInstructionsState.prototype.playMusic = function() {
	GameState.prototype.playMusic.call(this);
	
	if(!muted)
	{
		createjs.Sound.play("introductionMusic",{interrupt: createjs.Sound.INTERRUPT_ANY, loop:-1});
	}
}

BowmanInstructionsState.prototype.constructor = MageInstructionsState;
BowmanInstructionsState.prototype.setup = function()
{
	GameState.prototype.setup.call(this);
	var wClone = bowmanAnimations.clone();
	var aClone = bowmanAnimations.clone();
	var sClone = bowmanAnimations.clone();
	var dClone = bowmanAnimations.clone();
	var jClone = bowmanAnimations.clone();
	var jArrow = archerArrowAnimation.clone();
	var kClone = bowmanAnimations.clone();
	var kArrow = archerArrowAnimation.clone();
	var lClone = bowmanAnimations.clone();
	var lArrow = archerArrowAnimation.clone();
	wClone.gotoAndPlay("WalkUp");
	wClone.x = 160;
	wClone.y = 145;
	aClone.gotoAndPlay("WalkLeft");
	aClone.x = 160;
	aClone.y = 205;
	sClone.gotoAndPlay("WalkDown");
	sClone.x = 160;
	sClone.y = 265;
	dClone.gotoAndPlay("WalkRight");
	dClone.x = 160;
	dClone.y = 325;
	jClone.gotoAndPlay("AttackAll");
	jClone.x = 160;
	jClone.y = 385;
	kClone.gotoAndPlay("AttackAll");
	kClone.x = 160;
	kClone.y = 445;
	lClone.gotoAndPlay("AttackAll");
	lClone.x = 160;
	lClone.y = 505;
	stage.addChild(bowmanInstructionsScreen);
	stage.addChild(instructionsButton, mainMenuButton,wClone,
                  aClone,sClone,dClone,jClone,kClone, lClone);
    
}

function GameOverState()
{
   this.setup();
}
GameOverState.prototype = Object.create(GameState.prototype);
GameOverState.prototype.playMusic = function(){
    GameState.prototype.playMusic.call(this);

    if(!muted)
    {
        createjs.Sound.play("gameOverMusic",{interrupt: createjs.Sound.INTERRUPT_ANY, loop:-1});
    }
}


GameOverState.prototype.constructor = GameOverState;
GameOverState.prototype.setup = function()
{
    stage.addChild((playerWon) ?  gameoverWinScreen:  gameoverScreen ); 
    stage.addChild(playAgainButton);
    stage.addChild(mainMenuButton);
}
function DialogBox(animation)
{
    this.image = dialogImage;
    this.playerImage = dialogSprite.clone();
    this.playerImage.gotoAndPlay(animation);
    this.image.regX = 12;
    this.image.regY = 14;
    this.text = new createjs.Text("");
    this.text.regX = -128;
    this.text.regY = -17;
    this.text.font="24px Arial"

    this.container = new createjs.Container();
    this.container.addChild(this.image);
    this.container.addChild(this.text);
    this.container.addChild(this.playerImage);
    this.container.x = 12;
    this.container.y = (640-128);
    

    this.setText = function(textString)
    {
        this.text.text = textString; 
    }
    
    this.setPlayerImage = function(player)
    {
        this.playerImage.gotoAndPlay(player.animationName);
    }
}
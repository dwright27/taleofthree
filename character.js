function Character(animation)
{
    this.animation = animation;
    this.position = new Vector2D();
}


Character.prototype.update = function()
{
    this.animation.x = this.position.x;
    this.animation.y = this.position.y;
}
function Item(image)
{
    this.image = image;
    this.position = new Vector2D();
    this.image.on('click', function(){player.useItem(this)});
    
}

Item.prototype.addResources = function()
{
    stage.addChild(this.image);
}

Item.prototype.removeResources = function()
{
    stage.removeChild(this.image);
}

Item.prototype.use = function(player)
{
    player.useItem(this);
            for(var i in items)
        {
            if(items[i] == this)
            {
                
                items.splice(i, 1);
                
            }
        }
}

Item.prototype.update = function()
{
    this.image.x = this.position.x;
    this.image.y = this.position.y;
}
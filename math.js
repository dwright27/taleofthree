function generateRandomCharacter(c1,c2)
{
    var index1  = c1.charCodeAt(0); 
    var index2 = c2.charCodeAt(0);
    
    var randomNumber = (Math.random() * (index2 - index1)) + index1;
    
    var character = String.fromCharCode(Math.floor( randomNumber));
    return character;
}


function randomInt(min,max)
{
    return Math.floor((Math.random() * max-min) + min);
}
function Warrior()
{
    //warrior animation 
    Player.call(this,warriorAnimation.clone(),"warrior", warriorFallAnimation.clone());
    this.BASE_HEALTH = 100;
    this.health = this.BASE_HEALTH;
    this.sheildUp = false;
    this.attackRange = 64;
    this.attackPower = 5;
    this.thief = thiefAnimation.clone();
    
    
    this.block = function()
    {
        switch(this.lastDirection)
        {
                case KEYCODE_LEFT:
                    this.animation.gotoAndPlay("BlockLeft");
                break;
                
                case KEYCODE_RIGHT:
                    this.animation.gotoAndPlay("BlockRight");
                break;
                
                case KEYCODE_UP:
                    this.animation.gotoAndPlay("BlockUp");
                break;
                
                case KEYCODE_DOWN:
                    this.animation.gotoAndPlay("BlockDown");
                break;
        }
      // this.keyDown = true;
        this.isStanding = false;
        this.velocity.x = 0; this.velocity.y = 0;
        this.isDown = this.isUp =this.isRight = this.isLeft = false;
    };
    
    
    this.handleEnemyCollision = function()
    {
        var directionalRange = new Vector2D(0,0);
        switch(this.lastDirection)
            {
                    case KEYCODE_LEFT:
                        directionalRange.x = -this.attackRange/2;
                    break;

                    case KEYCODE_RIGHT:
                        directionalRange.x = this.attackRange/2;
                    break;

                    case KEYCODE_UP:
                        directionalRange.y = -this.attackRange/2;
                    break;

                    case KEYCODE_DOWN:
                        directionalRange.y = this.attackRange/2;

                    break;
            }
        directionalRange = directionalRange.add(this.position);
        
        for(var i =0; i < enemies.length; i++)
        {
            this.checkCollision(enemies[i],directionalRange);
        }
    };
    
    this.checkCollision = function(enemy, rangePosition)
    {
        if(enemy.position.distance(rangePosition) < this.attackRange/2)
        {
            enemy.attacked(this.attackPower);
            
        }
    }
    
    
}


Warrior.prototype = Object.create(Player.prototype);
Warrior.prototype.constructor = Warrior;


Warrior.prototype.attack = function()
{
     createjs.Sound.play("swordSound");
    Player.prototype.attack.call(this);
            this.handleEnemyCollision();

}

Warrior.prototype.addResources = function()
{
    Player.prototype.addResources.call(this);
   // stage.addChild(this.thief);
}

Warrior.prototype.update = function()
{
    //the warrior is special call Player.prototype.update for everyoneelse 
    
    Character.prototype.update.call(this);
    //this.thief.x = this.position.x;    this.thief.y = this.position.y;   
   
    this.checkCollisions();
    //Player.prototype.update.call(this);
    if(!this.sheildUp)
        this.move();
};

Warrior.prototype.handleKeyUp = function(keyup)
{
    
    if(!this.sheildUp)
    {
        Player.prototype.handleKeyUp.call(this,keyup);
    }
    switch(keyup)
    {

            case KEYCODE_Z:
                this.attack();
                break;
            case KEYCODE_X:
                this.keyDown = false;
                this.sheildUp = false;
                if(this.isStanding)
                this.stand();
                else
                    this.walk();
            break;
    }

    
};

Warrior.prototype.attacked = function(attackPower)
{
    if(!this.sheildUp)
    {
        Player.prototype.attacked.call(this,attackPower);

    }
    else
    {
         createjs.Sound.play("shieldSound");
    }
};

Warrior.prototype.handleKeyDown = function(keyDown)
{
    if(!this.sheildUp)
    {
        Player.prototype.handleKeyDown.call(this, keyDown);
    }
    switch(keyDown)
    {
        case KEYCODE_X:
                //shield 

            
                this.block();
                this.sheildUp = true;
            break;       
    }
};
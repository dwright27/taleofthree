//Tilestuffs
var caveSprites;
var roomSize = 20;
var Levels;
var Tiles;
var TilesInfo;
var currentLevel = 0;
var TileSizeX = 32;
var TileSizeY = 32;
var Tile_NOT_PASSABLE = "X";
var Tile_DOOR = "O";

var tileSets = Array();

function initTiles()
{
	initLevels();
	Tiles = new Array(roomSize);
	TilesInfo = new Array(roomSize);
    
    var caveSprite =new createjs.Sprite(caveSprites);
    
    tileSets.push(castleExteriorAnimation.clone());    
    tileSets.push(caveSprite.clone());
    tileSets.push(caveSprite.clone());
    tileSets.push(caveSprite.clone());
    tileSets.push(castleExteriorAnimation.clone());
    tileSets.push(castleInteriorAnimation.clone());
    tileSets.push(castleInteriorAnimation.clone());
    tileSets.push(castleInteriorAnimation.clone());       tileSets.push(castleInteriorAnimation.clone());



    
	for (var i = 0; i < roomSize; i++) 
	{
		Tiles[i] = new Array(roomSize);
		TilesInfo[i] = new Array(roomSize);
	}
	
	var caveSprite =new createjs.Sprite(caveSprites);
	for (var i = 0; i < roomSize; i++) 
	{
		for (var j = 0; j < roomSize; j++) 
		{
			Tiles[i][j] = caveSprite.clone();
			Tiles[i][j].x = TileSizeX*i;
			Tiles[i][j].y = (TileSizeY*j);
			stage.addChild(Tiles[i][j]);
		}
	}
	setTilesToLevel(0);
}

function setTileToImage(image)
{
    for (var i = 0; i < roomSize; i++) 
	{
		for (var j = 0; j < roomSize; j++) 
		{
            stage.removeChild(Tiles[i][j]);
        }
    }
    
    for (var i = 0; i < roomSize; i++) 
	{
		for (var j = 0; j < roomSize; j++) 
		{
			Tiles[i][j] = image.clone();
			Tiles[i][j].x = TileSizeX*i;
			Tiles[i][j].y = (TileSizeY*j);
			stage.addChild(Tiles[i][j]);
		}
	}
    
	if(player != null) {
    	player.removeResources();
		player.addResources();
	}
    
}

function initLevels()
{
	Levels = new Array(10);
	for (var i = 0; i < 10; i++)
	{
		Levels[i] = new Array(roomSize*2);
	}
	
	 Levels[0][0]  ="cccbcccbcdefgcbcccbc";
    Levels[0][1]  = "jjjijhjijklmnjijhjij";
    Levels[0][2]  = "qqqpqoqpqrstuqpqoqpq";
    Levels[0][3]  = "xxxwxvxwxyzABxwxvxwx";
    Levels[0][4]  = "CCCCCCCCCCMNCCCCCCCC";
	Levels[0][5]  = "CCCCCCCCCCOPCCCCCCCC";
    Levels[0][6]  = "CCCCCCCCCCOPCCCCCCCC";
    Levels[0][7]  = "CCCCCCCCCCOPCCCCCCCC";
    Levels[0][8]  = "CCCCCCCCCCJKCCCCCCCC";
    Levels[0][9]  = "CCCCCCCCCCOPCCCCCCCC";
    Levels[0][10] = "CCCCCCCCCCOPCCCCCCCC";
    Levels[0][11] = "CCCCCCCCCCOPCCCCCCCC";
    Levels[0][12] = "CCCCCCCCCCOPCCCCCCCC";
    Levels[0][13] = "CCCCCCCCCCOPCCCCCCCC";
    Levels[0][14] = "CCCCCCCCCCOPCCCCCCCC";
    Levels[0][15] = "CCCCCCCCCCOPCCCCCCCC";
    Levels[0][16] = "CCCCCCCCCCOPCCCCCCCC";
    Levels[0][17] = "CCCCCCCCCCOPCCCCCCCC";
    Levels[0][18] = "CCCCCCCCCCOPCCCCCCCC";
    Levels[0][19] = "CCCCCCCCCCOPCCCCCCCC";

	
	Levels[0][20] = "XXXXXXXXXXXXXXXXXXXX";
	Levels[0][21] = "XXXXXXXXXXXXXXXXXXXX";
	Levels[0][22] = "XXXXXXXXXXXXXXXXXXXX";
	Levels[0][23] = "XXXXXXXXXXXXXXXXXXXX";
	Levels[0][24] = "XXXXXXXXXX   XXXXXXX";
	Levels[0][25] = "XXXXXXXXXX   XXXXXXX";
	Levels[0][26] = "XXXXXXXXXX   XXXXXXX";
	Levels[0][27] = "XXXXXXXXXX   XXXXXXX";
	Levels[0][28] = "XXXXXXXXXX111XXXXXXX";
	Levels[0][29] = "XXXXXXXXXX   XXXXXXX";
	Levels[0][30] = "XXXXXXXXXX   XXXXXXX";
	Levels[0][31] = "XXXXXXXXXX   XXXXXXX";
	Levels[0][32] = "XXXXXXXXXX   XXXXXXX";
	Levels[0][33] = "XXXXXXXXXX   XXXXXXX";
	Levels[0][34] = "XXXXXXXXXX   XXXXXXX";
	Levels[0][35] = "XXXXXXXXXX   XXXXXXX";
	Levels[0][36] = "XXXXXXXXXX   XXXXXXX";
	Levels[0][37] = "XXXXXXXXXX   XXXXXXX";
	Levels[0][38] = "XXXXXXXXXX   XXXXXXX";
	Levels[0][39] = "XXXXXXXXXXXXXXXXXXXX";
	
	
	Levels[1][0]  = "FHHHHHHHHYHHHHHHHHHF";
	Levels[1][1]  = "HaaaaaaSaaaaaaaaaaaH";
	Levels[1][2]  = "HaaaaaaaaaaaaaaaaaaH";
	Levels[1][3]  = "HaaaaaaaaGaaaaaaaaaH";
	Levels[1][4]  = "HaaataaaaaaaaSaataaH";
	Levels[1][5]  = "HaaaAaaaaaaaaaaaAaaH";
	Levels[1][6]  = "HaaaaaaaaaaaaaaaaaaH";
	Levels[1][7]  = "HaaaGaaaaaaaaaaaaaaH";
	Levels[1][8]  = "HaaaaaaaaaaaaaaaaaaH";
	Levels[1][9]  = "HaaaaaSaaaaaaaaGaaaH";
	Levels[1][10] = "HaaaaaaaaaaaaaaaaaaH";
	Levels[1][11] = "HaaaaaaaaaaaGaaaaaSH";
	Levels[1][12] = "HaaaaaaGaaaaaaaaaaaH";
	Levels[1][13] = "HaaataaaaaaaaaaataaH";
	Levels[1][14] = "HaaaAaaaaaaaaaaaAaaH";
	Levels[1][15] = "HSaaaaaaaaaaaaaaaaaH";
	Levels[1][16] = "HaaaaaaaaaaaaaaGaaaH";
	Levels[1][17] = "HaaaaGaaaaaaaaaaaaaH";
	Levels[1][18] = "HaaaaaaaaaaaaaSaaaaH";
	Levels[1][19] = "FHHHHHHHHHHHHHHHHHHF";
	
	Levels[1][20] = "XXXXXXXXX2XXXXXXXXXX";
	Levels[1][21] = "X                  X";
	Levels[1][22] = "X       S2         X";
	Levels[1][23] = "X                  X";
	Levels[1][24] = "X                  X";
	Levels[1][25] = "X   X           X  X";
	Levels[1][26] = "X                  X";
	Levels[1][27] = "X                  X";
	Levels[1][28] = "X                  X";
	Levels[1][29] = "X   B     B     B  X";
	Levels[1][30] = "X                  X";
	Levels[1][31] = "X                  X";
	Levels[1][32] = "X                  X";
	Levels[1][33] = "X                  X";
	Levels[1][34] = "X   X           X  X";
	Levels[1][35] = "X                  X";
	Levels[1][36] = "X                  X";
	Levels[1][37] = "X                  X";
	Levels[1][38] = "X                S0X";
	Levels[1][39] = "XXXXXXXXXXXXXXXXXXXX";
    
    Levels[2][0]  = "FIHHIHIHHHIHIHIHRIHF";
	Levels[2][1]  = "HaaaaaSaaaaaaaaaYaaH";
	Levels[2][2]  = "IaaaaaaaaaaaaaaaaaaH";
	Levels[2][3]  = "IaaaaaaaaaaaaaaaaaaH";
	Levels[2][4]  = "HaaaaaaaaaaaaaaaaaaI";
	Levels[2][5]  = "IHHHHHHHHHHHHHHHHaaH";
	Levels[2][6]  = "HHHHHHHHHHHHHHHHHaaI";
	Levels[2][7]  = "HHHHHHHHHHHHHHHHHaaH";
	Levels[2][8]  = "HHHHHHHHHHHHHHHHHaaH";
	Levels[2][9]  = "HaaaaaaaaaaaaaZaaaaH";
	Levels[2][10] = "HaaaaaaaaaaaaaaaaaaI";
	Levels[2][11] = "IaaaaHHHHHHHHHHHHHHH";
	Levels[2][12] = "HaaaaHHHHHHHHHHHHHHH";
	Levels[2][13] = "IaaaaHHHHHHHHHHHHHHH";
	Levels[2][14] = "HaaaaHHHHHHHHHHHHHHH";
	Levels[2][15] = "HaaaaHHHHHHHHHHHHHHH";
	Levels[2][16] = "IaaaaaaaaaaaaaaaaaaH";
	Levels[2][17] = "HaaaaaaaaaaaaaaaaaaH";
	Levels[2][18] = "HaaaaZaaaaaaaaaaaaaH";
	Levels[2][19] = "FHHIHHIHHIIHHIHHRHHF";
	
	Levels[2][20] = "XXXXXXXXXXXXXXXX3XXX";
	Levels[2][21] = "XX                 X";
	Levels[2][22] = "XX             S3  X";
	Levels[2][23] = "XX                 X";
	Levels[2][24] = "XXM        M       X";
	Levels[2][25] = "XX                 X";
	Levels[2][26] = "XXXXXXXXXXXXXXXXX  X";
	Levels[2][27] = "X               X  X";
	Levels[2][28] = "XXXXXXXXXXXXXXXXX  X";
	Levels[2][29] = "X         M        X";
	Levels[2][30] = "X  M            M  X";
	Levels[2][31] = "X                  X";
	Levels[2][32] = "X    XXXXXXXXXXXXXXX";
	Levels[2][33] = "X    X             X";
	Levels[2][34] = "X    X             X";
	Levels[2][35] = "X    XXXXXXXXXXXXXXX";
	Levels[2][36] = "X         M        X";
	Levels[2][37] = "X   M              X";
	Levels[2][38] = "X              S1  X";
	Levels[2][39] = "XXXXXXXXXXXXXXXX11XX";
    

    
    Levels[3][0]  = "ccccccccccgccccccccc";
	Levels[3][1]  = "FFaaaaaaaaaaaaaaaaaF";
	Levels[3][2]  = "FFaaaaaaaaaaaaaaaaaF";
	Levels[3][3]  = "FFFaaaaaaaaaaaaaaaFF";
	Levels[3][4]  = "FFFFaaaaaaaaaaaaaFFF";
	Levels[3][5]  = "FFFFFFaaaaaaaaaFFFFF";
	Levels[3][6]  = "FFFFFFFaaaaaaaFFFFFF";
	Levels[3][7]  = "FFFFFFFaaaaaaaFFFFFF";
	Levels[3][8]  = "FFFFFFFaaaaaaaFFFFFF";
	Levels[3][9]  = "FFFFFFFaaaaaaaFFFFFF";
	Levels[3][10] = "FFFFFFFaaaaaaaFFFFFF";
	Levels[3][11] = "FFFFFFFaaaaaaaFFFFFF";
	Levels[3][12] = "FFFFFFFaaaaaaaFFFFFF";
	Levels[3][13] = "FFFFFFFaaaaaaaFFFFFF";
	Levels[3][14] = "FFFFFFFaaaaaaaFFFFFF";
	Levels[3][15] = "FFFFFFFaaaaaaaFFFFFF";
	Levels[3][16] = "FFFFFFFaaaaaaaFFFFFF";
	Levels[3][17] = "FFFFFFFaaaaaaaFFFFFF";
	Levels[3][18] = "FFFFFFFaaaaaaaFFFFFF";
	Levels[3][19] = "FFFFFFFaaaRaaaFFFFFF";
	
	Levels[3][20] = "XXXXXXXXXX4XXXXXXXXX";
	Levels[3][21] = "XX       S4        X";
	Levels[3][22] = "XX                 X";
	Levels[3][23] = "XXX    B      B   XX";
	Levels[3][24] = "XXXX             XXX";
	Levels[3][25] = "XXXXX      B    XXXX";
	Levels[3][26] = "XXXXXXX       XXXXXX";
	Levels[3][27] = "XXXXXXX  M M  XXXXXX";
	Levels[3][28] = "XXMXXXX       XXXMXX";
	Levels[3][29] = "XXXXXXX  M M  XXXXXX";
	Levels[3][30] = "XXXXXXX       XXXXXX";
	Levels[3][31] = "XXXXXXX       XXXXXX";
	Levels[3][32] = "XXXXXXX  M M  XXXXXX";
	Levels[3][33] = "XXXXXXX       XXXXXX";
	Levels[3][34] = "XXXXXXX       XXXXXX";
	Levels[3][35] = "XXXXXXX       XXXXXX";
	Levels[3][36] = "XXXXXXX       XXXXXX";
	Levels[3][37] = "XXXXXXX       XXXXXX";
	Levels[3][38] = "XXXXXXX  S2   XXXXXX";
	Levels[3][39] = "XXXXXXXXXX2XXXXXXXXX";

    Levels[4][0]  = "abcbcbcbdefgcbcbcbca";
    Levels[4][1]  = "hijijijiklmnjijijijh";
    Levels[4][2]  = "opqpqpqprstuqpqpqpqo";
    Levels[4][3]  = "vwxwxwxwyzABxwxwxwxv";
    Levels[4][4]  = "CaaaaaaaaaaaaaaaaaaC";
    Levels[4][5]  = "CaaaaaaaaMNaaaaaaaaC";
    Levels[4][6]  = "CCCCCCCCCOPCCCCCCCCC";
	Levels[4][7]  = "CCCCCCCCCOPCCCCCCCCC";
    Levels[4][8]  = "CaaaaaaaaVWaaaaaaaaC";
    Levels[4][9]  = "CaaaaaaaaaaaaaaaaaaC";
    Levels[4][10] = "CaaaaaaaaaaaaaaaaaaC";
    Levels[4][11] = "CaaaaaaaaaaaaaaaaaaC";
    Levels[4][12] = "CaaaaaaaaaaaaaaaaaaC";
    Levels[4][13] = "CaaaaaaaaaaaaaaaaaaC";
    Levels[4][14] = "CaaaaaaaaaaaaaaaaaaC";
    Levels[4][15] = "CaaaaaaaaaaaaaaaaaaC";
    Levels[4][16] = "CaaaaaaaaaaaaaaaaaaC";
    Levels[4][17] = "CaaaaaaaaaaaaaaaaaaC";
    Levels[4][18] = "CaaaaaaaaaaaaaaaaaaC";
    Levels[4][19] = "FFFFFFFFFFFQFFFFFFFF";

	
	Levels[4][20] = "XXXXXXXXXXXXXXXXXXXX";
	Levels[4][21] = "XXXXXXXXXXXXXXXXXXXX";
	Levels[4][22] = "XXXXXXXXXXXXXXXXXXXX";
	Levels[4][23] = "XXXXXXXXX555XXXXXXXX";
	Levels[4][24] = "X       S5         X";
	Levels[4][25] = "X   W          W   X";
	Levels[4][26] = "XXXXXXXXX   XXXXXXXX";
	Levels[4][27] = "XXXXXXXXX   XXXXXXXX";
	Levels[4][28] = "X                  X";
	Levels[4][29] = "X         W        X";
	Levels[4][30] = "X   M         M    X";
	Levels[4][31] = "X                  X";
	Levels[4][32] = "X         M        X";
	Levels[4][33] = "X                  X";
	Levels[4][34] = "X                  X";
	Levels[4][35] = "X                  X";
	Levels[4][36] = "X                  X";
	Levels[4][37] = "X                  X";
	Levels[4][38] = "X         S3       X";
	Levels[4][39] = "XXXXXXXXXXX3XXXXXXXX";
    
	Levels[5][0]  = "ededededefgdedededed";
	Levels[5][1]  = "lklklklklmnklklklklk";
	Levels[5][2]  = "srsrsrsrstursrsrsrsr";
	Levels[5][3]  = "zyzyzyzyzAByzyzyzyzy";
    Levels[5][4]  = "hhhhhhhhjiijhhhhhhhh";
	Levels[5][5]  = "ccccccccqppqcccccccc";
	Levels[5][6]  = "vvvvvvvvxwwxvvvvvvvv";
	Levels[5][7]  = "EaaaaaaaaaaaaaaaaaaE";
	Levels[5][8]  = "EaaaaaaaaaaaaaaaaaaE";
	Levels[5][9]  = "EaaaaaaaaaaaaaaaaaaE";
	Levels[5][10] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[5][11] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[5][12] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[5][13] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[5][14] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[5][15] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[5][16] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[5][17] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[5][18] = "ECCCCCCCCCCCCCCCaCCE";
	Levels[5][19] = "JJJJJJJJJJJJJJJJRJJJ";
	
	Levels[5][20] = "XXXXXXXXXXXXXXXXXXXX";
	Levels[5][21] = "XXXXXXXXXXXXXXXXXXXX";
	Levels[5][22] = "XXXXXXXXXXXXXXXXXXXX";
	Levels[5][23] = "XXXXXXXXX66XXXXXXXXX";
	Levels[5][24] = "XXXXXXXXX   XXXXXXXX";
	Levels[5][25] = "XXXXXXXXXS6 XXXXXXXX";
	Levels[5][26] = "XXXXXXXXX   XXXXXXXX";
	Levels[5][27] = "X                  X";
	Levels[5][28] = "X                  X";
	Levels[5][29] = "X         W        X";
	Levels[5][30] = "X    W          W  X";
	Levels[5][31] = "X                  X";
	Levels[5][32] = "X                  X";
	Levels[5][33] = "X                  X";
	Levels[5][34] = "X                  X";
	Levels[5][35] = "X                  X";
	Levels[5][36] = "X                  X";
	Levels[5][37] = "X                  X";
	Levels[5][38] = "X              S4  X";
	Levels[5][39] = "XXXXXXXXXXXXXXXX44XX";
    
    
    Levels[6][0]  = "ededededefgdedededed";
	Levels[6][1]  = "lklklklklmnklklklklk";
	Levels[6][2]  = "srsrsrsrstursrsrsrsr";
	Levels[6][3]  = "zyzyzyzyzAByzyzyzyzy";
	Levels[6][4]  = "EaaaaaaaaaaaaaaaaaaE";
	Levels[6][5]  = "EaaaaaaaaaaaaaaaaaaE";
	Levels[6][6]  = "EaaaaaaaaaaaaaaaaaaE";
	Levels[6][7]  = "EaaaaaaaaaaaaaaaaaaE";
	Levels[6][8]  = "EaaaaaaaaaaaaaaaaaaE";
	Levels[6][9]  = "EaaaaaaaaaaaaaaaaaaE";
	Levels[6][10] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[6][11] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[6][12] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[6][13] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[6][14] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[6][15] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[6][16] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[6][17] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[6][18] = "ECCCCCCCCCCCCCCCaCCE";
	Levels[6][19] = "JJJJJJJJJJJJJJJJRJJJ";
	
	Levels[6][20] = "XXXXXXXXXXXXXXXXXXXX";
	Levels[6][21] = "XXXXXXXXXXXXXXXXXXXX";
	Levels[6][22] = "XXXXXXXXXXXXXXXXXXXX";
	Levels[6][23] = "X88XXXXXXX77XXXXXXXX";
	Levels[6][24] = "X                  X";
	Levels[6][25] = "XS8        W       X";
	Levels[6][26] = "X        W   W     X";
	Levels[6][27] = "X                  X";
	Levels[6][28] = "X      W  W  W  W  X";
	Levels[6][29] = "X                  X";
	Levels[6][30] = "X                  X";
	Levels[6][31] = "X                  X";
	Levels[6][32] = "X                  X";
	Levels[6][33] = "X                  X";
	Levels[6][34] = "X                  X";
	Levels[6][35] = "X                  X";
	Levels[6][36] = "X                  X";
	Levels[6][37] = "X                  X";
	Levels[6][38] = "X              S5  X";
	Levels[6][39] = "XXXXXXXXXXXXXXXX55XX";
    
    Levels[7][0]  = "EJJJJJJJJJJJJJJJJJJE";
	Levels[7][1]  = "EaaaaaaaaaaaaaaaaaaE";
	Levels[7][2]  = "EaaaaaaaaaaaaaaaaaaE";
	Levels[7][3]  = "EaaaaaaaaaaaaaaaaaaE";
	Levels[7][4]  = "EaaaaaaaaaaaaaaaaaaE";
	Levels[7][5]  = "EaaaaaaaaaaaaaaaaaaE";
	Levels[7][6]  = "JaaaaaaaaaaaaaaaaaaJ";
	Levels[7][7]  = "hhhhhhhhhjijhhhhhhhh";
	Levels[7][8]  = "cccccccccqpqcccccccc";
	Levels[7][9]  = "vvvvvvvvvxwxvvvvvvvv";
	Levels[7][10] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[7][11] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[7][12] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[7][13] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[7][14] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[7][15] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[7][16] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[7][17] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[7][18] = "ECCCCCCCCCaaCCCCCCCE";
	Levels[7][19] = "JJJJJJJJJJDDJJJJJJJJ";
	
	Levels[7][20] = "XXXXXXXXXXXXXXXXXXXX";
	Levels[7][21] = "X                  X";
	Levels[7][22] = "X                  X";
	Levels[7][23] = "X         T        X";
	Levels[7][24] = "X                  X";
	Levels[7][25] = "X                  X";
	Levels[7][26] = "X                  X";
	Levels[7][27] = "XXXXXXXXXX XXXXXXXXX";
	Levels[7][28] = "XXXXXXXXXX XXXXXXXXX";
	Levels[7][29] = "XXXXXXXXXX XXXXXXXXX";
	Levels[7][30] = "X                  X";
	Levels[7][31] = "X                  X";
	Levels[7][32] = "X                  X";
	Levels[7][33] = "X                  X";
	Levels[7][34] = "X                  X";
	Levels[7][35] = "X                  X";
	Levels[7][36] = "X                  X";
	Levels[7][37] = "X                  X";
	Levels[7][38] = "X         S6       X";
	Levels[7][39] = "XXXXXXXXXXXXXXXXXXXX";
    
    Levels[8][0]  = "EJJJJJJJJJJJJJJJJJJE";
	Levels[8][1]  = "EaaaaaaaaaaaaaaaaaaE";
	Levels[8][2]  = "EaaaaaaaaaaaaaaaaaaE";
	Levels[8][3]  = "EaaaaaaaaaaaaaaaaaaE";
	Levels[8][4]  = "EaaaaaaaaaaaaaaaaaaE";
	Levels[8][5]  = "EaaaaaaaaaaaaaaaaaaE";
	Levels[8][6]  = "EaaaaaaaaaaaaaaaaaaE";
	Levels[8][7]  = "EaaaaaaaaaaaaaaaaaaE";
	Levels[8][8]  = "EaaaaaaaaaaaaaaaaaaE";
	Levels[8][9]  = "EaaaaaaaaaaaaaaaaaaE";
	Levels[8][10] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[8][11] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[8][12] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[8][13] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[8][14] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[8][15] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[8][16] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[8][17] = "EaaaaaaaaaaaaaaaaaaE";
	Levels[8][18] = "ECCCCCCCCCaaCCCCCCCE";
	Levels[8][19] = "JJJJJJJJJJaaJJJJJJJJ";
	
	Levels[8][20] = "XXXXXXXXXXXXXXXXXXXX";
	Levels[8][21] = "X                  X";
	Levels[8][22] = "X         P        X";
	Levels[8][23] = "X                  X";
	Levels[8][24] = "X      P    P      X";
	Levels[8][25] = "X                  X";
	Levels[8][26] = "X    P   P    P    X";
	Levels[8][27] = "X                  X";
	Levels[8][28] = "X                  X";
	Levels[8][29] = "X    P   P    P    X";
	Levels[8][30] = "X                  X";
	Levels[8][31] = "X                  X";
	Levels[8][32] = "X    P   P    P    X";
	Levels[8][33] = "X                  X";
	Levels[8][34] = "X        P         X";
	Levels[8][35] = "X                  X";
	Levels[8][36] = "X                  X";
	Levels[8][37] = "X         S6       X";
	Levels[8][38] = "X                  X";
	Levels[8][39] = "XXXXXXXXXX66XXXXXXXX";
    
}

function convertToDoor(level)
{
    return parseInt(level);
}

function isDoor(level, location)
{
    var number = parseInt(level);
    
    var returnValue = !isNaN(number);
    
    if(returnValue)
    {
        returnValue = Levels[currentLevel][location.y + roomSize][location.x-1] != 'S';
    }
    
    return returnValue;
}


function getSpawnPoint(fromLevel, toLevel)
{
    var returnPosition = new Vector2D(0,0);
    for(var i = 20; i < 40; i++)
    {
        for(var j in Levels[toLevel][i])
        {
            if(Levels[toLevel][i][j] == "S" )
            {
                var nextIndex = parseInt(j)+ 1;
                var character  =Levels[toLevel][i][nextIndex];
                var number = parseInt(character);
                if(number == fromLevel)
                {
                    returnPosition.x = j * TileSizeX;
                    returnPosition.y = (i - roomSize) * TileSizeY;
                }
            }
        }
    }
        
    return returnPosition;
}

function getEnemySpawnPoint(x,y)
{
    return new Vector2D((x) * TileSizeX , (y -roomSize) * TileSizeY);
}

function clearEnemies()
{
        var size = enemies.length;
    for(var i = 0; i < size; i++)
    {
        enemies[i].removeResources();
    }
    enemies.length = 0;
}

function spawnEnemies( level)
{
    clearEnemies();
    for(var i = 20; i < 40; i++)
    {
        for(var j in Levels[level][i])
        {
            if(Levels[level][i][j] == "B" )
            {
                var enemy = new StationaryEnemy(banditAnimation.clone(), getEnemySpawnPoint(j,i), arrowAnimation.clone());
                enemies.push(enemy);
            }
            else if(Levels[level][i][j] == "M" )
            {
                var enemy = new MobileEnemy(getEnemySpawnPoint(j,i));
                enemies.push(enemy);
            }
            else if(Levels[level][i][j] == "W" )
            {
                var enemy = new WarriorEnemy(getEnemySpawnPoint(j,i));
                enemies.push(enemy);
            }
            else if(Levels[level][i][j] == "T" )
            {
                var enemy = new Boss(getEnemySpawnPoint(j,i));
                enemies.push(enemy);
            }
            else if (Levels[level][i][j] == "P" )
            {
                var potion = new Potion(10);
                potion.position = getEnemySpawnPoint(j,i);
                potion.addResources();
                items.push(potion);
            }
        }
    }
    
    addEnemiesToStage();
}

function setTilesToLevel(levelNumber)
{
    setTileToImage(tileSets[levelNumber]);
    spawnEnemies(levelNumber);
	for (var i = 0; i < roomSize; i++) 
	{
		for (var j = 0; j < roomSize; j++) 
		{
			Tiles[i][j].gotoAndPlay(Levels[levelNumber][j].charAt(i));
			TilesInfo[i][j] = Levels[levelNumber][j+roomSize].charAt(i);
		}
	}
	
}

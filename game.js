//input codes
var KEYCODE_LEFT = 37;
var KEYCODE_UP = 38;
var KEYCODE_RIGHT = 39;
var KEYCODE_DOWN = 40;

var KEYCODE_W = 87;
var KEYCODE_A = 65;
var KEYCODE_D = 68;
var KEYCODE_S = 83;
var KEYCODE_M = 77;
var KEYCODE_Q = 81;
var KEYCODE_J = 74;
var KEYCODE_K = 75;
var KEYCODE_L = 76;

var KEYCODE_Z = 90;
var KEYCODE_X = 88;
var KEYCODE_C = 67;

var KEYCODE_1 = 49;
var KEYCODE_2 = 50;
var KEYCODE_3 = 51;

var KEYCODE_SPACE = 32;

var jKeyPressed = false;

//createjs constants
var FPS = 30;
var player;

var enemy; 

var potionImage;

//events 
document.onkeydown = handleKeyDown;
document.onkeyup = handleKeyUp;

//images
var backgroundScreen;
var titleScreen;
var instructionsScreen;
var selectionScreen;
var warriorInstructionsScreen;
var mageInstructionsScreen;
var bowmanInstructionsScreen;
var gameoverScreen;
var gameoverWinScreen;
var creditScreen;

var dialogSprite;

var warriorAnimation;
var thiefAnimation;
var archerAnimation;
var mageAnimation;

var slimeAnimation;
var banditAnimation;
var arrowAnimation;
var rainAnimation;

var healthDisplay;
var healthSprites;
var buttonDisplay;
//var buttonSprites;

var dialogBox;// = new DialogBox();

//game variables
var gameState = null;
var muted = false;

var faceImages;
var foodAnimation;
var mageSpellAnimations;

var buttonAnimation;
var castleExteriorAnimation;
var castleInteriorAnimation;

var floatingSkullAnimation;
var skeletonAnimation;

var rainDrops = new Array();
var numberOfRainDrops = 300;
var warriorFallAnimation;
var bowmanFallAnimation;
var mageFallAnimation;

var manifest = 
    [
        {src:"assets/images/title.jpg", id:"title"},
        {src:"assets/images/instructions.jpg", id:"instructions"},
		{src:"assets/images/selection.jpg", id:"selection"},
		{src:"assets/images/warriorInstructions.jpg", id:"warriorInstructions"},
		{src:"assets/images/mageInstructions.jpg", id:"mageInstructions"},
		{src:"assets/images/bowmanInstructions.jpg", id:"bowmanInstructions"},
        {src:"assets/images/credits.jpg", id :"credits"},
        {src:"assets/images/gameover.jpg", id:"gameover"},        {src:"assets/images/gameoverWin.jpg", id:"gameoverwin"},
        {src:"assets/images/food.png", id:"food"},
        {src:"assets/music/background.ogg", id:"backgroundMusic"},                    
		
        {src:"assets/music/introduction.ogg", id:"introductionMusic"},
        {src:"assets/music/shooting.wav", id:"arrowSound"}, 
        {src:"assets/music/swordSwing.wav", id:"swordSound"}, 
        {src:"assets/music/shieldBlock.wav", id:"shieldSound"},
        {src:"assets/music/fire.wav", id:"fireSound"}, {src:"assets/music/freeze.wav", id:"freezeSound"},

        {src:"assets/images/warrior.png", id : "warrior"},        {src:"assets/images/Wizard.png", id : "mage"},
        {src:"assets/images/rainDrop.png", id:"rainDrop"},
		{src:"assets/images/PowerCrystal.png", id : "bandit"},
		{src:"assets/images/CrystalLaser.png", id : "arrows"},	
        {src:"assets/images/FloatingSkull.png", id: "floatingSkull"},
        {src:"assets/images/Arrows.png", id : "archerArrows"},

        {src:"assets/images/skeletonwarrior.png", id:"skeletonWarrior"},
        
        {src:"assets/images/cave.png", id:"caveTiles"},        
        {src:"assets/images/CastleExterior.png", id:"castleEx"},
        {src:"assets/images/CastleInterior.png", id:"castleIn"},

		{src:"assets/images/heart.png", id:"healthHeart"},
        {src:"assets/images/Spells.png", id:"spells"},
		{src:"assets/images/buttons.png", id:"buttons"},
        {src:"assets/images/warriorFall.png", id:"warriorFall"},                {src:"assets/images/archerFall.png", id:"archerFall"},
        {src:"assets/images/mageFall.png", id:"mageFall"},

        {src:"assets/images/fallBackground.png", id:"fallScreen"},
        {src:"assets/images/bowman.png", id:"bowman"},
        {src:"assets/images/Thief.png", id:"thief"}
    ];

if( !!(window.addEventListener)) {
    window.addEventListener ("DOMContentLoaded", initialize);
}else{ //MSIE
    window.attachEvent("onload", initialize);
}

function initialize()
{
    initializeCreateJS();
    initializeMouseTracking();
    loadAssets();
    
}

function initializeCreateJS()
{
    canvas = document.getElementById("game");
    context = canvas.getContext('2d');
    canvas.width = 640;
    canvas.height = 640;
    stage = new createjs.Stage(canvas);
    createjs.Ticker.addEventListener("tick", gameLoop);
    createjs.Ticker.setFPS(FPS);
}

var fallScreenImage;
var playButton;
var instructionsButton;
var warriorInstructionsButton;
var mageInstructionsButton;
var bowmanInstructionsButton;
var selectWarriorButton;
var selectMageButton;
var selectBowmanButton;
var creditsButton;
var mainMenuButton;

var bowmanAnimations;
var thiefAnimation;
        
var enemies = new Array();
var items = new Array();
        
var buttonColor = "#FG0";
var buttonOutline = "#000";
var buttonTextColor = "#FFF";
var buttonTextSize = 24;
var buttonWidth;  
var buttonHeight; 
    
var fontColor = "#FFF";
var font = "Arial";
var fontSize = "24px"
var standardFont = fontSize + " " + font;
        
var transitioning = false;

function initializeUI()
{
    buttonWidth  = canvas.width * 0.3;
    buttonHeight = canvas.height * 0.1;    
    
    playButton = createButton(canvas.width * 0.2 ,canvas.height * 0.85, "Play", function(){ gameState = new SelectCharacterState(); gameState.setup();});
    instructionsButton = createButton(canvas.width * 0.8 ,canvas.height * 0.85,"Instructions", function(){ gameState = new InstructionsState(); });
	
	warriorInstructionsButton = createButton(canvas.width * 0.5, canvas.height * 0.85, "Warrior", function(){ gameState = new WarriorInstructionsState(); } );
	mageInstructionsButton = createButton(canvas.width * 0.2, canvas.height * 0.85, "Mage", function(){ gameState = new MageInstructionsState(); } );
	bowmanInstructionsButton = createButton(canvas.width * 0.8, canvas.height * 0.85, "Bowman", function(){ gameState = new BowmanInstructionsState(); } );
		
	selectWarriorButton = createButton(canvas.width * 0.5, canvas.height * 0.85, "Warrior", function(){ gameState = new StoryState("Warrior"); });
	selectMageButton = createButton(canvas.width * 0.2, canvas.height * 0.85, "Mage", function(){ gameState = new StoryState("Mage"); } );
	selectBowmanButton = createButton(canvas.width * 0.8, canvas.height * 0.85, "Bowman", function(){ gameState = new StoryState("Bowman"); } );
	
    playAgainButton = createButton(canvas.width * 0.5 ,canvas.height * 0.75, "PlayAgain",function(){ gameState = new SelectCharacterState();});
    creditsButton =  createButton(canvas.width * 0.5 ,canvas.height * 0.92, "Credits", function(){ gameState = new CreditsState();});                                                                           
    mainMenuButton = createButton(canvas.width * 0.5 ,canvas.height * 0.92, "MainMenu", function(){gameState = new TitleState();});
    
      //  player = new Mage();
        //fireball instead of aoe
//player = new Mage(mageSpellAnimations);
    
    //dialogBox = new DialogBox();
}
        
//function initializeEnemies()
//{
//    
//    var size = enemies.length;
//    for(var i = 0; i < size; i++)
//    {
//        enemies[i].removeResources();
//    }
//    enemies.length = 0;
//    spawnEnemies();
//    
//}
        
        
function removeRainDrops()
{
    for(var i in rainDrops)
    {
        stage.removeChild(rainDrops[i].animation);
    }
    rainDrops.length = 0;
}

function addRainDrops()
{
    removeRainDrops();
    for(var i = 0; i < numberOfRainDrops; i++)
    {
        rainDrops.push(new RainDrop());
        rainDrops[i].reset();
        stage.addChild(rainDrops[i].animation);
    }
}

function updateRainDrops()
{
    for(var i in rainDrops)
    {
        rainDrops[i].update();
    }
}

function removeEnemy(enemy)
{
            for(var i in enemies)
        {
            if(enemies[i] == enemy)
            {
                enemies.splice(i, 1);
                 
            }
        }
}

function addEnemiesToStage()
{
    for(var i in enemies)
    {
        enemies[i].addResources();
    }
    
}

function isValidPosition(position)
{
    var valid = true;
    var tpos = new Vector2D(position.x,position.y);
    tpos.x = Math.floor(tpos.x / TileSizeX);
    tpos.y = Math.floor(tpos.y / TileSizeY);
    
    
    
    return TilesInfo[tpos.x][tpos.y] != Tile_NOT_PASSABLE;
}

function generateEnemyPosition()
{
    var enemyPosition = new Vector2D(0,0);
    
    do
    {
        enemyPosition.x =Math.random() * (600 - 40) + 40;
        enemyPosition.y = Math.random() * (600 - 40) + 40;
    }while(!isValidPosition(enemyPosition));

    return enemyPosition;
}
//
//function spawnEnemies()
//{
//	var numberOfEnemies = Math.floor(Math.random() * (3) + 3);
//    
//	for(var i = 0; i < numberOfEnemies; i++)
//	{	
//		var enemyClone = new StationaryEnemy(banditAnimation.clone(),generateEnemyPosition(), arrowAnimation.clone());
//		enemies.push(enemyClone)
//	}
//
//}

function createButton(x,y,text,onclick, width, height, color, outline,  textSize,textColor)
{
    var button = buttonAnimation.clone();
    //button.graphics.beginFill((color === undefined) ? buttonColor : color);  
    button.gotoAndPlay(text);
	button.x = x;
	button.y = y;
    
//    button.graphics.drawRect(x ,y, (width === undefined)? buttonWidth : width,(height === undefined)? buttonHeight : height);
//    button.regX = ((width === undefined)? buttonWidth : width)/2;
//    button.regY  = ((height === undefined)? buttonHeight : height) / 2;
 

//    var text = new createjs.Text(text);
//    text.color = "#fff";//(textColor === undefined) ? buttonTextColor : textColor;
//    text.textAlign = 'center';
//    text.textBaseline = 'middle';
//    text.x = x;
//    text.y = y;
//    
//    text.font="24px Arial";//((textSize === undefined) ? buttonTextSize : textSize)+ "px " + font;
//    button.txt = text;
    button.addEventListener("click", onclick);
    
    return button;
}

function transition()
{
    stage.addChild(fallScreenImage);
    stage.addChild(player.fallAnimation);
    player.fallAnimation.x = canvas.width/2;
    player.fallAnimation.y = canvas.height/2;
    player.fallAnimation.gotoAndPlay("Fall");
}

function loadAssets()
{
    assetQueue  =new createjs.LoadQueue(true, "" );
    assetQueue.installPlugin(createjs.Sound);
    assetQueue.on("complete", loadAssetsComplete, this);
    assetQueue.loadManifest(manifest);
}


function loadAssetsComplete(event)
{
    titleScreen = new createjs.Bitmap(assetQueue.getResult("title"));
	selectionScreen = new createjs.Bitmap(assetQueue.getResult("selection"));
    instructionScreen = new createjs.Bitmap(assetQueue.getResult("instructions"));
	warriorInstructionsScreen = new createjs.Bitmap(assetQueue.getResult("warriorInstructions"));
	mageInstructionsScreen = new createjs.Bitmap(assetQueue.getResult("mageInstructions"));
	bowmanInstructionsScreen = new createjs.Bitmap(assetQueue.getResult("bowmanInstructions"));
    gameoverScreen = new createjs.Bitmap(assetQueue.getResult("gameover"));    gameoverWinScreen = new createjs.Bitmap(assetQueue.getResult("gameoverwin"));

    backgroundScreen = new createjs.Bitmap(assetQueue.getResult("background"));  
    creditScreen = new createjs.Bitmap(assetQueue.getResult("credits"));  
	potionImage = new createjs.Bitmap(assetQueue.getResult("potion"));
    fallScreenImage = new createjs.Bitmap(assetQueue.getResult("fallScreen"));
    loadAnimations();
    initializeUI();
    initTiles();
    gameState = new TitleState();
}

function loadAnimations()
{
    var foodImages = new createjs.SpriteSheet(
        {
            images:[assetQueue.getResult("food")],
            frames: {width:29, height: 23, regX:0, regY:0},
            animations:
            {
                a: [0,0, "a"],
                b: [1,1, "b"],
                c: [2,2, "c"],
                d: [3,3, "d"],
                e: [4,4, "e"],
                f: [5,5, "f"],
                g: [6,6, "g"],
                h: [7,7, "h"],
                i: [8,8, "i"],
                j: [9,9, "j"],
                k: [10,10, "k"],
                l: [11,11, "l"],
                m: [12,12, "m"],
                n: [13,13, "n"],
                o: [14,14, "o"],
                p: [15,15, "p"],
                q: [16,16, "q"],
                r: [17,17, "r"],
                s: [18,18, "s"],
                t: [19,19, "t"]
            }
        
        }
    );
    
    foodAnimation =new createjs.Sprite(foodImages); 
    
    var skeletonWarrior = new createjs.SpriteSheet(
        {
            images : [assetQueue.getResult("skeletonWarrior")],
            frames: {width: 64, height: 64, regX :32, regY: 47},
		  animations: 
		  {
			StandDown: [0,0, "StandDown" , 0.2],
            StandLeft: [4,4, "StandLeft", 0.2],
            StandRight: [8,8, "StandRight", 0.2],
            StandUp: [12,12, "StandUp", 0.2],
			WalkDown: [0,3, "WalkDown", 0.2],
			WalkLeft: [4,7, "WalkLeft", 0.2],
			WalkRight: [8,11, "WalkRight", 0.2],
			WalkUp: [12,15, "WalkUp", 0.2],
			AttackDown: [16,19, "StandDown"] ,
			AttackLeft: [20,23, "StandLeft"],
			AttackRight: [24,27, "StandRight"],
			AttackUp: [28,31, "StandUp"]
            }
        }
    );
    skeletonAnimation = new createjs.Sprite(skeletonWarrior);
    
    
    var rainDropSprite = new createjs.SpriteSheet(
        {
            images:[assetQueue.getResult("rainDrop")],
           frames: {width: 7, height: 12, regX :0, regY: 0},
              animations: 
              {
                  Fall:[0,0,"Fall"],
                  Drop:[0,4,"Land", 0.2],
                  Land:[4,4,"Land"]
              }
        }
    );
    
    rainAnimation = new createjs.Sprite(rainDropSprite);
    
    warriorSprites = new createjs.SpriteSheet({
		images: [assetQueue.getResult("warrior")], 
		frames: {width: 64, height: 64, regX :32, regY: 47},
		animations: 
		{
			StandDown: [0,0, "StandDown" , 0.2],
            StandLeft: [4,4, "StandLeft", 0.2],
            StandRight: [8,8, "StandRight", 0.2],
            StandUp: [12,12, "StandUp", 0.2],
			WalkDown: [0,3, "WalkDown", 0.2],
			WalkLeft: [4,7, "WalkLeft", 0.2],
			WalkRight: [8,11, "WalkRight", 0.2],
			WalkUp: [12,15, "WalkUp", 0.2],
			BlockDown: [1,1, "BlockDown", 0.2],
			BlockLeft: [5,5, "BlockLeft", 0.2],
			BlockRight: [9,9, "BlockRight", 0.2],
			BlockUp: [13,13, "BlockUp", 0.2],
			AttackDown: [16,19, "StandDown"],
			AttackLeft: [20,23, "StandLeft"],
			AttackRight: [24,27, "StandRight"],
			AttackUp: [28,31, "StandUp"],
            AttackMoveDown: [16,19, "WalkDown"],
			AttackMoveLeft: [20,23, "WalkLeft"],
			AttackMoveRight: [24,27, "WalkRight"],
			AttackMoveUp: [28,31, "WalkUp"],
			AttackAll: [16, 31, "AttackAll", 0.25],
			BlockAll: {
				frames:[1, 5, 9, 13],
				next: "BlockAll",
				speed: 0.05
			}
		}});
    warriorAnimation = new createjs.Sprite(warriorSprites);
    
        var bowmanSprite = new createjs.SpriteSheet({
		images: [assetQueue.getResult("bowman")], 
		frames: {width: 64, height: 64, regX :32, regY: 47},
		animations: 
		{
			StandDown: [0,0, "StandDown" , 0.2],
            StandLeft: [4,4, "StandLeft", 0.2],
            StandRight: [8,8, "StandRight", 0.2],
            StandUp: [12,12, "StandUp", 0.2],
			WalkDown: [0,3, "WalkDown", 0.2],
			WalkLeft: [4,7, "WalkLeft", 0.2],
			WalkRight: [8,11, "WalkRight", 0.2],
			WalkUp: [12,15, "WalkUp", 0.2],
			AttackDown: [16,19, "StandDown"],
			AttackLeft: [20,23, "StandLeft"],
			AttackRight: [24,27, "StandRight"],
			AttackUp: [28,31, "StandUp"],
            AttackMoveDown: [16,19, "WalkDown"],
			AttackMoveLeft: [20,23, "WalkLeft"],
			AttackMoveRight: [24,27, "WalkRight"],
			AttackMoveUp: [28,31, "WalkUp"],
			AttackAll: [16, 31, "AttackAll", 0.25],
			}
        });
    bowmanAnimations = new createjs.Sprite(bowmanSprite);
    
     var thief = new createjs.SpriteSheet({
		images: [assetQueue.getResult("thief")], 
		frames: {width: 32, height: 48, regX :16, regY: 24},
		animations: 
		{
			StandDown: [0,0, "StandDown" , 0.2],
            StandLeft: [4,4, "StandLeft", 0.2],
            StandRight: [8,8, "StandRight", 0.2],
            StandUp: [12,12, "StandUp", 0.2],
			WalkDown: [0,3, "WalkDown", 0.2],
			WalkLeft: [4,7, "WalkLeft", 0.2],
			WalkRight: [8,11, "WalkRight", 0.2],
			WalkUp: [12,15, "WalkUp", 0.2],
			}
        });
    thiefAnimation = new createjs.Sprite(thief);
    
    
    var floatingSkullSprite = new createjs.SpriteSheet
    (
        {
            images: [assetQueue.getResult("floatingSkull")],
            frames: {width: 32, height: 48, regX :16, regY: 44},
            animations :
            {
            StandDown: [0,0, "StandDown" ],
            StandLeft: [1,1, "StandLeft"],
            StandRight: [2,2, "StandRight"],
            StandUp: [3,3, "StandUp"]
            }
        }
    );
    
    floatingSkullAnimation = new createjs.Sprite(floatingSkullSprite);
    
    var mageSprite = new createjs.SpriteSheet({
		images: [assetQueue.getResult("mage")], 
		frames: {width: 64, height: 64, regX :32, regY: 47},
		animations: 
		{
			StandDown: [0,0, "StandDown" , 0.2],
            StandLeft: [4,4, "StandLeft", 0.2],
            StandRight: [8,8, "StandRight", 0.2],
            StandUp: [12,12, "StandUp", 0.2],
			WalkDown: [0,3, "WalkDown", 0.2],
			WalkLeft: [4,7, "WalkLeft", 0.2],
			WalkRight: [8,11, "WalkRight", 0.2],
			WalkUp: [12,15, "WalkUp", 0.2],
           	AttackDown: [16,19, "StandDown"],
			AttackLeft: [20,23, "StandLeft"],
			AttackRight: [24,27, "StandRight"],
			AttackUp: [28,31, "StandUp"],
            AttackMoveDown: [16,19, "WalkDown"],
			AttackMoveLeft: [20,23, "WalkLeft"],
			AttackMoveRight: [24,27, "WalkRight"],
			AttackMoveUp: [28,31, "WalkUp"],
			AttackAll: [16, 31, "AttackAll", 0.25],
		}});
    
    mageAnimation = new createjs.Sprite(mageSprite);
    
    //0 31 32 63
    spellsSprite = new createjs.SpriteSheet({
		images: [assetQueue.getResult("spells")], 
		frames: {width: 128, height: 128, regX :64, regY: 64},
		animations: 
        {
            standdown: [31,31,"standdown",2.5],
            fire:[32,63,"standdown",2.5],
            ice:[0,31,"standdown",2.5],
			FireLoop:[32,63,"FireLoop",2],
            IceLoop:[0,31,"IceLoop",2],
        }
    });
    
    mageSpellAnimations = new createjs.Sprite(spellsSprite);
	
	banditSprites = new createjs.SpriteSheet
    (
        {
        images: [assetQueue.getResult("bandit")], 
		frames: {width: 32, height: 48, regX :16, regY: 44},
		animations: 
		{
			StandDown: [0,0, "StandDown" , 0.2],
            PowerUp : [0,4,"PoweredUp",0.2],
            PoweredUp:[4,4,"PoweredUp"]
        }
        }
    );
    banditAnimation = new createjs.Sprite(banditSprites);
	
    
    archerArrowSprites = new createjs.SpriteSheet
    (
        {
        images: [assetQueue.getResult("archerArrows")], 
		frames: {width: 32, height: 32, regX :16, regY: 44},
		animations: 
		{
			fireUp: [0,0, "fireUp"],
            fireLeft: [3,3, "fireLeft"],
            fireRight: [2,2, "fireRight"],
            fireDown: [1,1, "fireDown"],
            iceUp: [8,8, "iceUp"],
            iceLeft: [11,11, "iceLeft"],
            iceRight: [10,10, "iceRight"],
            iceDown: [9,9, "iceDown"],
            regularUp: [16,16, "regularUp"],
            regularLeft: [19,19, "regularLeft"],
            regularRight: [17,17, "regularDown"],
            regularDown: [18,18, "regularRight"],

        }
        }
    );
    
    
    var warriorFallSprite = new createjs.SpriteSheet
    (
        
        {images:[assetQueue.getResult("warriorFall")],
        frames: [[0,0,189,190,0,98.45,117.4],[189,0,181,182,0,94.45,113.4],[0,190,163,164,0,85.45,104.4],[163,190,153,154,0,80.45,99.4],[316,190,141,142,0,74.45,93.4],[0,354,130,130,0,69.45,87.4],[130,354,119,118,0,63.45,81.4],[249,354,109,110,0,58.45,77.4],[358,354,95,95,0,51.45,70.4],[0,484,83,84,0,45.45,64.4],[83,484,71,72,0,39.45,58.400000000000006],[154,484,62,62,0,35.45,53.400000000000006],[216,484,55,56,0,31.450000000000003,50.400000000000006],[271,484,41,41,0,24.450000000000003,43.400000000000006],[271,484,41,41,0,24.450000000000003,43.400000000000006]],
        animations : 
         {
         Fall:[0,14, "Stop", 0.5],
         Stop:[14,14, "Stop"]
        }
        }

    );
    
    warriorFallAnimation = new createjs.Sprite(warriorFallSprite);
    
        var bowmanFallSprite = new createjs.SpriteSheet
    (
        
        {images:[assetQueue.getResult("archerFall")],
        frames:  [[0,0,215,214,0,108.4,161.95],[215,0,201,200,0,101.4,154.95],[0,214,193,192,0,97.4,150.95],[193,214,179,179,0,90.4,144.95],[0,406,165,165,0,83.4,137.95],[165,406,151,151,0,76.4,130.95],[316,406,141,141,0,71.4,125.94999999999999],[0,571,127,127,0,64.4,118.94999999999999],[127,571,113,113,0,57.400000000000006,111.94999999999999],[240,571,103,103,0,52.400000000000006,106.94999999999999],[343,571,87,87,0,44.400000000000006,98.94999999999999],[430,571,67,67,0,34.400000000000006,88.94999999999999],[0,698,59,59,0,30.400000000000006,84.94999999999999],[59,698,47,47,0,24.400000000000006,78.94999999999999],[59,698,47,47,0,24.400000000000006,78.94999999999999]],
        animations : 
         {
         Fall:[0,14, "Stop", 0.5],
         Stop:[14,14, "Stop"]
        }
        }

    );
    
    bowmanFallAnimation = new createjs.Sprite(bowmanFallSprite);
    
        var mageFallSprite = new createjs.SpriteSheet
    (
        
        {images:[assetQueue.getResult("mageFall")],
        frames:  [[0,0,219,218,0,112.35,175.85],[219,0,207,206,0,106.35,169.85],[0,218,193,192,0,99.35,162.85],[193,218,175,174,0,90.35,153.85],[0,410,155,154,0,80.35,143.85],[155,410,141,140,0,73.35,136.85],[296,410,129,128,0,67.35,130.85],[0,564,117,116,0,61.349999999999994,124.85],[117,564,107,106,0,56.349999999999994,119.85],[224,564,97,96,0,51.349999999999994,114.85],[321,564,85,84,0,45.349999999999994,108.85],[406,564,73,72,0,39.349999999999994,102.85],[0,680,59,58,0,32.349999999999994,95.85],[59,680,53,52,0,29.349999999999994,92.85],[59,680,53,52,0,29.349999999999994,92.85]],
        animations : 
         {
         Fall:[0,14, "Stop", 0.5],
         Stop:[14,14, "Stop"]
        }
        }

    );
    
    mageFallAnimation = new createjs.Sprite(mageFallSprite);
    
    archerArrowAnimation = new createjs.Sprite(archerArrowSprites);
	
    arrowSprites = new createjs.SpriteSheet
    (
        {
        images: [assetQueue.getResult("arrows")], 
		frames: {width: 32, height: 32, regX :16, regY: 44},
		animations: 
		{
			Up: [0,0, "Up" , 0.2],
            Left: [3,3, "Left", 0.2],
            Right: [2,2, "Right", 0.2],
            Down: [1,1, "Down", 0.2],
			UpLeft: [7,7, "UpLeft", 0.2],
			UpRight: [4,4, "UpRight", 0.2],
			DownLeft: [6,6, "DownLeft", 0.2],
			DownRight: [5,5, "DownRight", 0.2],
        }
        }
    );
    arrowAnimation = new createjs.Sprite(arrowSprites);
caveSprites = new createjs.SpriteSheet({
		images: [assetQueue.getResult("caveTiles")], 
		frames: {width: 32, height: 32, regX :0, regY: 0},
		animations: 
		{
			a: [0,0, "a"],
			b: [1,1, "b"],
			c: [2,2, "c"],
			d: [3,3, "d"],
			e: [4,4, "e"],
			f: [5,5, "f"],
			g: [6,6, "g"],
			h: [7,7, "h"],
			i: [8,8, "i"],
			j: [9,9, "j"],
			k: [10,10, "k"],
			l: [11,11, "l"],
			m: [12,12, "m"],
			n: [13,13, "n"],
			o: [14,14, "o"],
			p: [15,15, "p"],
			q: [16,16, "q"],
			r: [17,17, "r"],
			s: [18,18, "s"],
			t: [19,19, "t"],
			u: [20,20, "u"],
			v: [21,21, "v"],
			w: [22,22, "w"],
			x: [23,23, "x"],
			y: [24,24, "y"],
			z: [25,25, "z"],
			A: [26,26, "A"],
			B: [27,27, "B"],
			C: [28,30, "C"],
			D: [29,29, "D"],
			E: [30,30, "E"],
			F: [31,31, "F"],
			G: [32,32, "G"],
			H: [33,33, "H"],
			I: [34,34, "I"],
			J: [35,35, "J"],
			K: [36,36, "K"],
			L: [37,37, "L"],
			M: [38,38, "M"],
			N: [39,39, "N"],
			O: [40,40, "O"],
			P: [41,41, "P"],
			Q: [42,42, "Q"],
			R: [43,43, "R"],
			S: [44,44, "S"],
			T: [45,45, "T"],
			U: [46,46, "U"],
			V: [47,47, "V"],
			W: [48,48, "W"],
			X: [49,49, "X"],
			Y: [50,50, "Y"],
			Z: [51,51, "Z"],
		
		}
		});
    
    var castleExSprites = new createjs.SpriteSheet({
		images: [assetQueue.getResult("castleEx")], 
		frames: {width: 32, height: 32, regX :0, regY: 0},
		animations: 
		{
			a: [0,0, "a"],
			b: [1,1, "b"],
			c: [2,2, "c"],
			d: [3,3, "d"],
			e: [4,4, "e"],
			f: [5,5, "f"],
			g: [6,6, "g"],
			h: [7,7, "h"],
			i: [8,8, "i"],
			j: [9,9, "j"],
			k: [10,10, "k"],
			l: [11,11, "l"],
			m: [12,12, "m"],
			n: [13,13, "n"],
			o: [14,14, "o"],
			p: [15,15, "p"],
			q: [16,16, "q"],
			r: [17,17, "r"],
			s: [18,18, "s"],
			t: [19,19, "t"],
			u: [20,20, "u"],
			v: [21,21, "v"],
			w: [22,22, "w"],
			x: [23,23, "x"],
			y: [24,24, "y"],
			z: [25,25, "z"],
			A: [26,26, "A"],
			B: [27,27, "B"],
			C: [28,29, "C", 0.1],
           // D: [29,28, "D", 0.1],

			F: [31,31, "F"],
			G: [32,32, "G"],
			H: [33,33, "H"],
			I: [34,34, "I"],
			J: [35,35, "J"],
			K: [36,36, "K"],
			L: [37,37, "L"],
			M: [38,38, "M"],
			N: [39,39, "N"],
			O: [40,40, "O"],
			P: [41,41, "P"],
			Q: [42,42, "Q"],
			R: [43,43, "R"],
			S: [44,44, "S"],
			T: [45,45, "T"],
			U: [46,46, "U"],
			V: [47,47, "V"],
			W: [48,48, "W"],
			X: [49,49, "X"],
			Y: [50,50, "Y"],
			Z: [51,51, "Z"],
		
		}
		});

    castleExteriorAnimation = new createjs.Sprite(castleExSprites);
    
    
        var castleInSprites = new createjs.SpriteSheet({
		images: [assetQueue.getResult("castleIn")], 
		frames: {width: 32, height: 32, regX :0, regY: 0},
		animations: 
		{
			a: [0,0, "a"],
			b: [1,1, "b"],
			c: [2,2, "c"],
			d: [3,3, "d"],
			e: [4,4, "e"],
			f: [5,5, "f"],
			g: [6,6, "g"],
			h: [7,7, "h"],
			i: [8,8, "i"],
			j: [9,9, "j"],
			k: [10,10, "k"],
			l: [11,11, "l"],
			m: [12,12, "m"],
			n: [13,13, "n"],
			o: [14,14, "o"],
			p: [15,15, "p"],
			q: [16,16, "q"],
			r: [17,17, "r"],
			s: [18,18, "s"],
			t: [19,19, "t"],
			u: [20,20, "u"],
			v: [21,21, "v"],
			w: [22,22, "w"],
			x: [23,23, "x"],
			y: [24,24, "y"],
			z: [25,25, "z"],
			A: [26,26, "A"],
			B: [27,27, "B"],
			C: [28,28, "C"],
			D: [29,29, "D"],
			E: [30,30, "E"],
			F: [31,31, "F"],
			G: [32,32, "G"],
			H: [33,33, "H"],
			I: [34,34, "I"],
			J: [35,35, "J"],
			K: [36,36, "K"],
			L: [37,37, "L"],
			M: [38,38, "M"],
			N: [39,39, "N"],
			O: [40,40, "O"],
			P: [41,41, "P"],
			Q: [42,42, "Q"],
			R: [43,43, "R"],
			S: [44,44, "S"],
			T: [45,45, "T"],
			U: [46,46, "U"],
			V: [47,47, "V"],
			W: [48,48, "W"],
			X: [49,49, "X"],
			Y: [50,50, "Y"],
			Z: [51,51, "Z"],
		
		}
		});

    castleInteriorAnimation = new createjs.Sprite(castleInSprites);
    
healthSprites = new createjs.SpriteSheet({
		images: [assetQueue.getResult("healthHeart")],
		frames: {width: 116, height: 119, regX: 0, regY: 0},
		animations: {
            
            
            health100:[0,0,"health100"],
			health80:
            {
                frames: [0,1,2,3,2,1],
                next: "health80",
                speed: 0.2,
            },
            health60:
            {
                frames: [4,5,6,7,6,5,4],
                next: "health60",
                speed: 0.2,
            },
            health40:
            {
                frames: [8,9,10,11,10,9,8],
                next: "health40",
                speed: 0.2,
            },
            health20:
            {
                frames: [12,13,14,15,14,13,12],
                next: "health20",
                speed: 0.2,
            },
//			health95:[0, 1,  "health95", 0.05],
//			health90:[2, 4,  "health90", 0.10],
//			health85:[0, 3,  "health85", 0.15],
//			health80:[0, 4,  "health80", 0.20],
//			health75:[0, 5,  "health75", 0.25],
//			health70:[0, 6,  "health70", 0.30],
//			health65:[0, 7,  "health65", 0.35],
//			health60:[0, 8,  "health60", 0.40],
//			health55:[0, 9,  "health55", 0.45],
//			health50:[5, 10, "health50", 0.50],
//			health45:[0, 11, "health45", 0.55],
//			health40:[0, 12, "health40", 0.60],
//			health35:[0, 13, "health35", 0.65],
//			health30:[0, 14, "health30", 0.70],
//			health25:[0, 15, "health25", 0.75],
//			health20:[0, 16, "health20", 0.80],
//			health15:[0, 17, "health15", 0.85],
//			health10:[0, 18, "health10", 0.90],
//			health05:[0, 19, "health05", 0.95],
	}});
	healthDisplay = new createjs.Sprite(healthSprites);
	healthDisplay.y = canvas.height -119;
	
var buttonSprites = new createjs.SpriteSheet({
	images: [assetQueue.getResult("buttons")],
	frames: {width: 190, height: 44, regX: 95, regY: 0},
	animations: {
		Play:[0, 0, "Play"],
		MainMenu:[1, 1, "MainMenu"],
		Instructions:[2, 2, "Instructions"],
		Warrior:[3, 3, "Warrior"],
		Mage:[4, 4, "Mage"],
		Bowman:[5, 5, "Bowman"],
		Credits:[6, 6, "Credits"],
		PlayAgain:[7, 7, "PlayAgain"]
	}
});
	buttonAnimation = new createjs.Sprite(buttonSprites);
}

function initializeMouseTracking()
{
    stage.on("stagemousemove", 
             function(event)
             {
                 mouseX = Math.floor(event.stageX);
                 mouseY = Math.floor(event.stageY);
             }
            );
}

function gameLoop()
{
    if(gameState !== null)
        gameState.update();

    stage.update();
}

//handle user input    
function handleKeyUp()
{
    if(!evt){var evt = window.event;}
    switch(evt.keyCode)
    {
     case KEYCODE_UP:
            
            break;   
     case KEYCODE_DOWN : break;       
     case KEYCODE_LEFT: break;      
     case KEYCODE_RIGHT: break; 
     case KEYCODE_W :  break;   
     case KEYCODE_A : break;       
     case KEYCODE_S : break;   
     case KEYCODE_J : jKeyPressed = !jKeyPressed; break;
     case KEYCODE_D: break; 
     case KEYCODE_Q : gameState = new GameOverState();
        gameState.setup();break;
     case KEYCODE_SPACE :  break;

    }
    if(player)  
    player.handleKeyUp(evt.keyCode);
}

function handleKeyDown()
{
    if(!evt){var evt = window.event;}
    switch(evt.keyCode)
    {
     case KEYCODE_UP :  break;   
     case KEYCODE_DOWN :break;       
     case KEYCODE_LEFT : break;      
     case KEYCODE_RIGHT:break; 
     case KEYCODE_W :  break;   
     case KEYCODE_A : break;       
     case KEYCODE_S : break;      
     case KEYCODE_D: break; 
     case KEYCODE_M :
            muted = !muted;
            if(muted)
            {
                createjs.Sound.stop();
            }
            else
            {
                gameState.playMusic();
            }
            break;
     case KEYCODE_SPACE :  break;
    }
        
    if(player)  
    player.handleKeyDown(evt.keyCode);    
}
        


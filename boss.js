function Shard()
{
    this.animation = arrowAnimation.clone();
    this.position = new Vector2D(-32,-32);
    this.direction;
    this.distance = 0;
    this.chooseAnimation = function(direction)
    {
        this.animation.gotoAndPlay( direction);
    };
    
    this.reset = function()
    {
         this.position = new Vector2D(-32,-32);
    }
    
    this.update = function()
    {
        this.animation.x = this.position.x;
        this.animation.y = this.position.y;
    }
    
    this.addResources = function()
    {
        stage.addChild(this.animation);
    }
    
        this.removeResources = function()
    {
        stage.removeChild(this.animation);
    }
}

function Boss(position)
{
    Enemy.call(this,thiefAnimation.clone());
    this.attackPower = 15;

    this.attackRange = 800;
    this.speed = 5;
    this.range = 100;
    this.longRange = 200;
    this.sightRange = 200;
    this.health = 200;
    this.position = position;
    this.animation.gotoAndPlay("StandDown");
    this.LEFT = 0;
    this.RIGHT = 1;
    this.DOWN = 2;
    this.UP = 3;
    this.attackDelay = 0;
    this.ATTACK_DELAY_AMOUNT =2;
    this.lastDirection = this.RIGHT;
    this.skulls = new Array(4);
    this.crystals = new Array(3);    
    this.shardsTime = 4;
    this.shardsShot = false;
    this.arrowSpeed = 10;
    
    this.intitializeMinions = function()
    {
                
        for(var i =0 ;i < 4; i++ )
        {
            removeEnemy(this.skulls[i]);
        }
        
        for(var i =0 ;i < 4; i++ )
        {
            this.skulls[i] = new MobileEnemy(new Vector2D( -32,-32));
        
        }
        
        for(var i in this.crystals)
        {
            this.crystals[i].removeResources();
        }
        
        for(var i =0 ;i < 3; i++ )
        {
            this.crystals[i] = new Shard();
            this.crystals[i].position.x = 0;
            this.crystals[i].position.y = 0;
            this.crystals[i].addResources();
        }
    }
    
    
    this.getDirectionVelocity = function()
    {
        var velocity = new Vector2D(0,0);
        switch(this.arrow.direction)
        {
                case KEYCODE_DOWN:
                    velocity.y += this.arrowSpeed;
                break;
                case KEYCODE_LEFT:
                    velocity.x -= this.arrowSpeed;
                    
                break;
                case KEYCODE_RIGHT:
                    velocity.x += this.arrowSpeed;
                   
                break;
                case KEYCODE_UP:
                    velocity.y -= this.arrowSpeed;
                    
                break;
        }
        
        return velocity;
    };
    
    this.updateShards = function()
    {
        
        
        for(var i in this.crystals)
        {
        this.crystals[i].position = this.crystals[i].position.add(this.getDirectionVelocity(this.crystals[i]));
        this.crystals[i].update();
        this.crystals[i].distance+= this.arrowSpeed;
        }
        
        this.checkCollidedWithEnemy();
        if(this.crystals[i].distance >= this.attackRange)
        {
             for(var i in this.crystals)
            {
                this.crystals[i].distance = 0;
                this.crystals[i].reset();
            }
            this.shardsShot = false;
        }
    }
    
    this.checkCollidedWithEnemy = function()
    {
        for(var i = 0; i < this.crystals.length ;i ++ )
        {
            var enemy = enemies[i];
            if (player.position.distance(this.crystals[i].position) < TileSizeX)
            {
                player.attacked(this.attackPower);
                this.crystals[i].reset();
            }
        }
        
        //return collision;
    };
    
    this.summon = function()
    {
        
        for(var i =0 ;i < 4; i++ )
        {
            removeEnemy(this.skulls[i]);
        }
        
        for(var i =0 ;i < 4; i++ )
        {
            var s =this.skulls[i]; 
            this.skulls[i].health = this.skulls[i].BASE_HEALTH;
            
        }
        
        var size = 64;
        
        this.skulls[0].position.x = this.position.x - size;                   this.skulls[0].position.y = this.position.y - size;
        
            this.skulls[1].position.x = this.position.x + size;                   this.skulls[1].position.y = this.position.y - size;
        
            this.skulls[2].position.x = this.position.x + size;                   this.skulls[2].position.y = this.position.y + size;
        
            this.skulls[1].position.x = this.position.x - size;                   this.skulls[1].position.y = this.position.y + size;

        for(var i =0 ;i < 4; i++ )
        {
            this.skulls[i].addResources();
            enemies.push(this.skulls[i]);
        }
        
    }
    
    this.moveLeft = function()
    {
        if(this.lastDirection != this.LEFT)
        {
            this.lastDirection = this.LEFT;
            this.animation.gotoAndPlay("WalkLeft");
        }
        
        this.position = this.position.add({x:-this.speed,y:0});
    };
    this.moveRight = function()
    {
        if(this.lastDirection != this.RIGHT)
        {
            this.lastDirection = this.RIGHT;
            this.animation.gotoAndPlay("WalkRight");
        }
        
        this.position = this.position.add({x:this.speed,y:0});
    
    };
    this.direction = this.moveLeft;
    this.distanceTraveled = 0;
    this.DISTANCE_TO_TRAVEL = 100;
    this.moveTowardPlayer = function()
    {
        var deltaPos = player.position.subtract(this.position);
        
        if(Math.abs(deltaPos.x) > Math.abs(deltaPos.y))
        {
            if(deltaPos.x > 0)
            {
                if(this.lastDirection != this.RIGHT)
                {
                    this.animation.gotoAndPlay("StandRight");
                    this.lastDirection = this.RIGHT;
                }
               // this.position = this.position.add({x:this.speed,y:0});

            }
            else
            {
                if(this.lastDirection != this.LEFT)
                {
                    this.animation.gotoAndPlay("StandLeft");
                    this.lastDirection = this.LEFT;
                }
                 //               this.position = this.position.add({x:-this.speed,y:0});

            }
        }
        else
        {
            if(deltaPos.y > 0) 
            {
                if(this.lastDirection != this.DOWN)
                {
                    this.animation.gotoAndPlay("StandDown");
                    this.lastDirection = this.DOWN;
                }
                             //   this.position = this.position.add({x:0,y:this.speed});

            }
            else
            {
                if(this.lastDirection != this.UP)
                {
                    this.animation.gotoAndPlay("StandUp");
                    this.lastDirection = this.UP;
                }
               // this.position = this.position.add({x:0,y:-this.speed});
            }
        }
    }
     this.checkCollisions = function()
    {
        var tpos = new Vector2D(this.position.x,this.position.y);
        tpos.x = Math.floor(tpos.x / TileSizeX);
        tpos.y = Math.floor(tpos.y / TileSizeY);
        var t = TilesInfo[tpos.x-1][tpos.y];
         

            
                if ((this.position.x-(TileSizeX/2)) <= (tpos.x)*TileSizeX)
                {
                    if (TilesInfo[tpos.x-1][tpos.y] == Tile_NOT_PASSABLE)
                    {
                        this.position.x += this.speed;
                        //this.direction = this.moveRight;
                        //this.distanceTraveled = 0;
                    }
                }
     
                
        
    
                if ((this.position.x+(TileSizeX/2)) >= (tpos.x)*TileSizeX)
                {
                    if (TilesInfo[tpos.x+1][tpos.y] == Tile_NOT_PASSABLE)
                    {
                        this.position.x -= this.speed;
                        //this.direction = this.moveLeft;
                        //this.distanceTraveled = 0;
                    }
					
                }
            
                
            
                if ((this.position.y-(TileSizeY/2)) <= (tpos.y)*TileSizeY)
                {
                    if (TilesInfo[tpos.x][tpos.y-1] == Tile_NOT_PASSABLE)
                    {
                        this.position.y += this.speed;
                        
                    }
                }
            
                if ((this.position.y+(TileSizeY/2)) >= (tpos.y)*TileSizeY)
                {
                    if (TilesInfo[tpos.x][tpos.y+1] == Tile_NOT_PASSABLE)
                    {
                        this.position.y -= this.speed;
                        
                    }
                }
            
        
    }
     
     this.getDirectionVelocity = function(shard)
    {
        var velocity = new Vector2D(0,0);
        switch(shard.direction)
        {
                case 1:
                    velocity.y += this.arrowSpeed;
                break;
                case 0:
                    velocity.x -= this.arrowSpeed;
                    
                break;
                case 3:
                    velocity.x += this.arrowSpeed;
                   
                break;
                case 2:
                    velocity.y -= this.arrowSpeed;
                    
                break;
        }
        
        return velocity;
    };
     
     
     this.shoot = function()
     {
            for(var i in this.crystals)
            {
                this.crystals[i].distance = 0;
                this.crystals[i].reset();
            }
         
         
         this.crystals[0].chooseAnimation("Left");  
         this.crystals[0].direction =0;
         var direction = (player.position.y - this.position.y) > 0 ? "Down" : "Up";
         this.crystals[1].chooseAnimation(direction);
         this.crystals[1].direction = (player.position.y - this.position.y) > 0 ? 1:2;
         this.crystals[2].chooseAnimation("Right");
         this.crystals[2].direction =3;
         this.crystals[0].position.x = this.position.x;
        this.crystals[0].position.y = this.position.y;
                  this.crystals[1].position.x = this.position.x;
        this.crystals[1].position.y = this.position.y;
                  this.crystals[2].position.x = this.position.x;
        this.crystals[2].position.y = this.position.y;

        

     }
     this.attackInDirection = function()
     {
         switch(this.lastDirection)
         {
                 case this.LEFT:
                    this.animation.gotoAndPlay("StandLeft");
                 break;
                case this.RIGHT:
                    this.animation.gotoAndPlay("StandLeft");
                 break;
                case this.UP:
                    this.animation.gotoAndPlay("StandLeft");
                 break;
                case this.DOWN:
                    this.animation.gotoAndPlay("StandLeft");
                 break;
         }
     }
     
     
     this.intitializeMinions();
}


Boss.prototype = Object.create(Enemy.prototype);
Boss.prototype.constructor = Boss;

Boss.prototype.update = function()
{
    Enemy.prototype.update.call(this);
    if(this.position.distance(player.position) < this.sightRange )
    {
        this.attack();
    }
    else
    {
        this.move();
    }
}

Boss.prototype.move = function()
{
    this.checkCollisions();
    this.direction();
    this.distanceTraveled += this.speed;
    if(this.distanceTraveled > this.DISTANCE_TO_TRAVEL)
    {
        this.distanceTraveled = 0;
        this.direction = (this.lastDirection == this.LEFT) ? this.moveRight : this.moveLeft;
    }
    
}

Boss.prototype.attacked = function(attackPower)
{
    Enemy.prototype.attacked.call(this,attackPower);
    if(this.health <= 0)
    {
        playerWon = true;
        gameState = new GameOverState();
    }
}

Boss.prototype.attack = function()
{
    this.checkCollisions();
    if(player.position.distance(this.position) < this.range)
    {
        if(this.attackDelay <= 0)
        {
            this.shoot();
            this.shardsShot = true;
            this.attackDelay = this.ATTACK_DELAY_AMOUNT;
        }
        else
        {
            this.attackDelay-=(1/FPS);
        }
    }
    else if(player.position.distance(this.position) < this.longRange)
    {
        
        if(this.attackDelay <= 0)
        {
            this.summon();
            this.attackDelay = this.ATTACK_DELAY_AMOUNT;
        }
        else
        {
            this.attackDelay-=(1/FPS);
        }
    }
    
    if(this.shardsShot)
    {
        this.updateShards();
    }
    else
    {
        this.moveTowardPlayer();
    }
}


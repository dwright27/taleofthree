function Mage()
{
    //constructor
    //Player.call(this, mageAnimation,"mage");
    Player.call(this,mageAnimation.clone(),"mage",mageFallAnimation.clone());
    this.health = 75;
    this.BASE_HEALTH = 75;
    this.attackRange = 64;
    this.attackAnimations = mageSpellAnimations.clone();//attackAnimations.clone();
    this.attackAnimations.gotoAndPlay("standdown");
    this.attackPower = 7;
    
    this.fire = function()
    {
        createjs.Sound.play("fireSound");
        this.attack();
    }
    this.ice = function()
    {
        createjs.Sound.play("freezeSound");
        this.attack();
    }
}





Mage.prototype = Object.create(Player.prototype);
Mage.prototype.constructor = Mage;


Mage.prototype.attack  = function()
{
    
    Player.prototype.attack.call(this);
    for(var i =0; i < enemies.length; i++)
    {
        if(this.position.distance(enemies[i].position) < this.attackRange)
        {
            enemies[i].attacked(this.attackPower);
        }
    }
}

Mage.prototype.addResources = function()
{
    stage.addChild(this.attackAnimations);
    Player.prototype.addResources.call(this);
   
}

Mage.prototype.removeResources = function()
{
     stage.removeChild(this.attackAnimations);
    Player.prototype.removeResources.call(this);
}

Mage.prototype.update = function()
{
    Player.prototype.update.call(this);
    this.attackAnimations.x = this.position.x;
    this.attackAnimations.y = this.position.y;
}

Mage.prototype.handleKeyUp = function(keyup)
{
    Player.prototype.handleKeyUp.call(this,keyup);
    switch(keyup)
    {
            case KEYCODE_Z:
                //fire;
                this.attackAnimations.gotoAndPlay("fire");
                this.fire();
            break;
            case KEYCODE_X:
               this.attackAnimations.gotoAndPlay("ice");
            this.ice();
            break;
    }
    
    
    
}


Mage.prototype.handleKeyDown = function(keyDown)
{
    Player.prototype.handleKeyDown.call(this, keyDown);
    
}
function Archer()
{
    //constructor
    Player.call(this, bowmanAnimations.clone(), "archer", bowmanFallAnimation.clone());
    this.health = 50;
    this.BASE_HEALTH = 50;
    this.attackPower = 3;
    this.arrow = new Arrow();
    this.arrowSpeed = 30;
    this.attackRange = 250;
    this.shotArrow = false;
    
    this.shoot = function()
    {
        Player.prototype.attack.call(this);
        this.arrow.position = new Vector2D(this.position.x, this.position.y);
        this.shotArrow = true;
        createjs.Sound.play("arrowSound");
    };
    
    this.regular = function()
    {
        this.shoot();
        this.arrow.chooseAnimation("regular",this.getDirectionString());
    };
    this.fire = function()
    {
        this.shoot();
        this.arrow.chooseAnimation("fire",this.getDirectionString());
    };
    this.ice = function()
    {
        this.shoot();
        this.arrow.chooseAnimation("ice",this.getDirectionString());
    };
        
    this.getDirectionString = function()
    {
        switch(this.lastDirection)
        {
                case KEYCODE_DOWN:
                    return "Down";
                break;
                case KEYCODE_LEFT:
                    return "Left";
                break;
                case KEYCODE_RIGHT:
                    return "Right";
                break;
                case KEYCODE_UP:
                    return "Up";
                break;
        }
        
        return "Up";
    };
    this.getDirectionVelocity = function()
    {
        var velocity = new Vector2D(0,0);
        switch(this.arrow.direction)
        {
                case KEYCODE_DOWN:
                    velocity.y += this.arrowSpeed;
                break;
                case KEYCODE_LEFT:
                    velocity.x -= this.arrowSpeed;
                    
                break;
                case KEYCODE_RIGHT:
                    velocity.x += this.arrowSpeed;
                   
                break;
                case KEYCODE_UP:
                    velocity.y -= this.arrowSpeed;
                    
                break;
        }
        
        return velocity;
    };
    
    
    this.checkCollidedWithEnemy = function()
    {
        var collision  =false;
        
        for(var i = 0; i < enemies.length && !collision;i ++ )
        {
            var enemy = enemies[i];
            if (enemy.position.distance(this.arrow.position) < TileSizeX)
            {
                enemy.attacked(this.attackPower);
                this.resetArrow();
                collision = true;
            }
        }
        
        //return collision;
    };
    
    this.resetArrow = function()
    {
        this.arrow.position = new Vector2D(-32,-32);
        this.arrow.update();
        this.shotArrow = false;
        this.arrow.distance = 0;
    }
    
    this.updateArrow = function()
    {
        
        this.arrow.position = this.arrow.position.add(this.getDirectionVelocity());
        this.arrow.update();
        this.checkCollidedWithEnemy();
        this.arrow.distance+= this.arrowSpeed;
        if(this.arrow.distance >= this.attackRange)
        {
            this.resetArrow();
        }
        
    };
}


Archer.prototype = Object.create(Player.prototype);
Archer.prototype.constructor = Archer;

Archer.prototype.update = function()
{
    Player.prototype.update.call(this);
    if(this.shotArrow)
    {
        this.updateArrow();
    }
    
}

Archer.prototype.handleKeyUp = function(keyup)
{
    Player.prototype.handleKeyUp.call(this,keyup)
    if(!this.shotArrow)
    {
        this.arrow.direction = this.lastDirection;
    switch(keyup)
    {
            case KEYCODE_Z:
                this.regular();
            break;
            case KEYCODE_X:
                //fire arrow
                this.fire();
            break;
            case KEYCODE_C:
                //ice arrow 
                this.ice();
            break;
    }
    }
    
}

Archer.prototype.addResources = function()
{
    this.arrow.addResources();
    
    archerArrowAnimation.gotoAndPlay("fireRight");
    Player.prototype.addResources.call(this);
}

Archer.prototype.removeResources = function()
{
    this.arrow.removeResources();
    Player.prototype.removeResources.call(this);
}

Archer.prototype.handleKeyDown = function(keyDown)
{
    Player.prototype.handleKeyDown.call(this, keyDown); 
}


function Arrow()
{
    this.animation = archerArrowAnimation.clone();
    this.position = new Vector2D(-32,-32);
    this.direction;
    this.distance = 0;
    this.chooseAnimation = function(name,direction)
    {
        this.animation.gotoAndPlay(name + direction);
    };
    
    this.update = function()
    {
        this.animation.x = this.position.x;
        this.animation.y = this.position.y;
    }
    
    this.addResources = function()
    {
        stage.addChild(this.animation);
    }
    
        this.removeResources = function()
    {
        stage.removeChild(this.animation);
    }
}
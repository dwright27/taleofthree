function Potion(healthReward)
{
    //constructor...
    Item.call(this,foodAnimation.clone());
    this.image.gotoAndPlay(generateRandomCharacter("a", "t"));
    this.healthReward = healthReward;
    this.used = false;
}

Potion.prototype = Object.create(Item.prototype);
Potion.prototype.constructor = Potion;


Potion.prototype.use = function (player)
{
    player.health += this.healthReward;
    Item.prototype.use.call(this, player);
}

Potion.prototype.update = function()
{
    if(!this.used)
    {
    Item.prototype.update.call(this);
    this.checkCollision();
    }
}

Potion.prototype.checkCollision = function()
{
    
	var distance = Math.sqrt(Math.pow((this.image.x - player.position.x), 2) + Math.pow((this.image.y - player.position.y), 2));
	var intersection = ndgmr.checkRectCollision(this.image, player.animation);
	if(intersection !== null)
	   {
	   		this.use(player);
		   this.image.visible = false;
           this.used = true;
	   }
}
function Vector2D(x,y)
{
    this.x = x==undefined ? 0 : x;
    this.y = y==undefined ? 0 : y;
    this.add = function(v)
    {
       return new Vector2D(this.x + v.x, this.y + v.y); 
    }
    this.subtract= function(v)
    {
       return new Vector2D(this.x - v.x, this.y - v.y); 
    }
    
    this.multiply = function(scalar)
    {
        return new Vector2D(this.x * scalar, this.y * scalar); 
    }
    
    this.normalize = function()
    {
        var length = this.x * this.x + this.y * this.y;
        var returnVector = new Vector2D(this.x,this.y);
        if(length !== 0)
        {
            returnVector = returnVector.multiply(1/length);
        }
        else{
            returnVector.x = 0;
            returnVector.y = 0;
        }
        return returnVector;
    }
    
    this.distance = function( v2)
    {
        return  Math.sqrt(Math.pow(v2.x - this.x,2) + Math.pow(v2.y - this.y,2));
    }
}

//this.distance = function(v1, v2)
//{
//    return  Math.sqrt(Math.pow(v2.x - v1.x,2) + Math.pow(v2.y - v1.y,2));
//}


